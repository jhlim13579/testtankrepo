﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Mycsharpsockett
{
    public partial class CRoomManage
	{
        public static CRoomManage mInstance;
        public static CRoomManage Inst
        {
            get
            {
                if (null == mInstance)
                {
                    mInstance = new CRoomManage();
                    
                    return mInstance;
                }
                return mInstance;
			}
		}
		public CRoomManage()
		{
			if (null == mInstance)
			{
				mInstance = this;
			}
            SetDummyRoomDatas();
            SetPacketMemory();
        }
     
        public class PlayerWrapper 
        {
            public CPacket.playerinfo info;
            public CPacket.gamerstate gamerstates = new CPacket.gamerstate();
            public CPacket.gamersoldier gamersoldiers = new CPacket.gamersoldier();
            public SessionObject sessionObjRef;
            public int shotnum;
        }

        public class RoomWrapper
        {
            public CPacket.roominfo info = new CPacket.roominfo();
            public List<PlayerWrapper> playerInfoList = new List<PlayerWrapper>();
        
            
         
        }
        public List<RoomWrapper> mCAllRoomList = new List<RoomWrapper>();
        public List<PlayerWrapper> mCAllLobbyUserList = new List<PlayerWrapper>();
        public int testUserNum = 1;
        public int testUserIdx = 101;

        public Random rands = new Random();

        public string[] RandRoomName = new string[]
        {
            "안녕하세요~!",
            "한판하실 분 환영합니다.",
            "신나게 한 판 하자!",
            "화끈한 대결 하실 분!",
            "안들어오면 안됩니다.",
            "탁월한 승부사를 가리자!",
            "피하거나 즐기거나!",
            "제발 들어와줘!",
            "즐겜 하실분 구함!",
            "최강의 탱크를 가려보자",
            "뉴비 상대해줄분 구함~",
        };
       
        //패킷 메모리용
        CPacket.sc_login sc_login = new CPacket.sc_login();
        CPacket.sc_roomlist_info sc_roomlist_info = new CPacket.sc_roomlist_info();
        CPacket.sc_room_make sc_room_make = new CPacket.sc_room_make();
        CPacket.sc_room_into sc_room_into = new CPacket.sc_room_into();
        CPacket.sc_room_out sc_room_out = new CPacket.sc_room_out();
        CPacket.sc_room_ready sc_room_ready = new CPacket.sc_room_ready();
        CPacket.sc_room_start sc_room_start = new CPacket.sc_room_start();
        CPacket.sc_game_init sc_game_init = new CPacket.sc_game_init();

        CPacket.sc_game_move sc_game_move = new CPacket.sc_game_move();
        CPacket.sc_game_soldier sc_game_soldier = new CPacket.sc_game_soldier();
        CPacket.sc_game_hit sc_game_hit = new CPacket.sc_game_hit();

        public void SetPacketMemory()
        {
            sc_game_move.ph.PacketLength = (UInt16)Marshal.SizeOf(typeof(CPacket.sc_game_move));
            sc_game_move.ph.PacketId = (UInt16)CPacket.PACKET_INFO.sc_game_move;
            sc_game_move.state = new CPacket.gamerstate[10];

            sc_game_soldier.ph.PacketLength = (UInt16)Marshal.SizeOf(typeof(CPacket.sc_game_soldier));
            sc_game_soldier.ph.PacketId = (UInt16)CPacket.PACKET_INFO.sc_game_soldier;
            sc_game_soldier.state = new CPacket.gamersoldier[10];

            sc_game_hit.ph.PacketLength = (UInt16)Marshal.SizeOf(typeof(CPacket.sc_game_hit));
            sc_game_hit.ph.PacketId = (UInt16)CPacket.PACKET_INFO.sc_game_hit;

            sc_room_start.ph.PacketLength = (UInt16)Marshal.SizeOf(typeof(CPacket.sc_room_start));
            sc_room_start.ph.PacketId = (UInt16)CPacket.PACKET_INFO.sc_room_start;

            sc_room_out.ph.PacketLength = (UInt16)Marshal.SizeOf(typeof(CPacket.sc_room_out));
            sc_room_out.ph.PacketId = (UInt16)CPacket.PACKET_INFO.sc_room_out;

            sc_room_ready.ph.PacketLength = (UInt16)Marshal.SizeOf(typeof(CPacket.sc_room_ready));
            sc_room_ready.ph.PacketId = (UInt16)CPacket.PACKET_INFO.sc_room_ready;

            sc_game_init.ph.PacketLength = (UInt16)Marshal.SizeOf(typeof(CPacket.sc_game_init));
            sc_game_init.ph.PacketId = (UInt16)CPacket.PACKET_INFO.sc_game_init;
            sc_game_init.UserData = new CPacket.playerinfo[10];

            sc_room_into.ph.PacketLength = (UInt16)Marshal.SizeOf(typeof(CPacket.sc_room_into));
            sc_room_into.ph.PacketId = (UInt16)CPacket.PACKET_INFO.sc_room_into;
            sc_room_into.playerinfos = new CPacket.playerinfo[10];
            sc_room_into.ErrMsg = new byte[32];

            sc_room_make.ph.PacketLength = (UInt16)Marshal.SizeOf(typeof(CPacket.sc_room_make));
            sc_room_make.ph.PacketId = (UInt16)CPacket.PACKET_INFO.sc_room_make;

            sc_roomlist_info.header.PacketLength = (UInt16)Marshal.SizeOf(typeof(CPacket.sc_roomlist_info));
            sc_roomlist_info.header.PacketId = (UInt16)CPacket.PACKET_INFO.sc_roomlist_info;
            sc_roomlist_info.Info = new CPacket.roominfo[10];


            sc_login.ph.PacketLength = (UInt16)Marshal.SizeOf(typeof(CPacket.sc_login));
            sc_login.ph.PacketId = (UInt16)CPacket.PACKET_INFO.sc_login;


        }

        public void SetDummyRoomDatas() 
        {
            //룸데이터 24개정도 만들어봄
			for (int i = 0; i < 24; i++)
			{
                CPacket.roominfo roomData = new CPacket.roominfo();
                roomData.RoomNo = i;
                roomData.MaxUserNum = 10;
                roomData.RoomTitle = new byte[48];
              
                byte[] tmpTitle = Encoding.Unicode.GetBytes(RandRoomName[rands.Next(0, RandRoomName.Length)]); 
                for (int i2 = 0; i2 < roomData.RoomTitle.Length; i2++)
				{
                    if (tmpTitle.Length > i2)
                    {
                        roomData.RoomTitle[i2] = tmpTitle[i2];
                    }
                }
                mCAllRoomList.Add(new RoomWrapper { info = roomData });
                //CPacket.playerinfo tmpInfo = new CPacket.playerinfo();
                //mCAllRoomList.Last().playerInfoList.Add(new PlayerWrapper { info = tmpInfo });

            }
        
            
        }
      

        public void ReceiveLogout(SessionObject sessionObj, CPacket.cs_logout msg)
        {
            if (sessionObj == null)
            {
                Form1.Inst.DebugMsg("세션 널!");
                return;
            }

            OutProcess(sessionObj);

            sessionObj.isDead = true;
        }

        //로비에 있는 유저 데이터 초기화
		public void OutProcess(SessionObject sessionObj) 
        {
            //방에 들어가 있는 경우
			if (sessionObj.playerInfo.RoomNo >= 0)
			{
                RoomWrapper tmpRoomWrap = mCAllRoomList.ElementAt(sessionObj.playerInfo.RoomNo);
                
                //방에 유저 -1 한다.
                tmpRoomWrap.info.NowUserNum -= 1;

                //만약 방에 유저가 0일시 방 초기화(삭제) 수행
                if (tmpRoomWrap.info.NowUserNum <= 0)
                {
                    ResetRoom(sessionObj.playerInfo.RoomNo);
                }


                //방 안에있는 모두에게 브로드캐스트
                for (int i = 0; i < tmpRoomWrap.playerInfoList.Count; i++)
                {
                    if (tmpRoomWrap.playerInfoList[i].sessionObjRef.playerInfo.UserIdx != sessionObj.playerInfo.UserIdx)
                    {
                        CParser.Inst.Send(tmpRoomWrap.playerInfoList[i].sessionObjRef, CMarshal.Serialize(sc_room_out));
                    }
                }

                //바뀐 룸 정보에 대해 로비에 고지
                SendLobbyAllRoomList(sessionObj);
            }
			else
			{

			}
           
        }


		public void ResetRoom(int idx) 
        {
            mCAllRoomList.RemoveAt(idx);
        }

      
        public void ReceiveLogin(SessionObject sessionObj, CPacket.cs_login msg)
        {
            if (sessionObj == null)
            {
                Form1.Inst.DebugMsg("세션 널!");
                return;
            }

            sessionObj.playerInfo = new CPacket.playerinfo();
            sessionObj.playerInfo.UserID = new byte[32];
            string name = "tstId" + testUserIdx.ToString() + ".";
            byte[] tmpTitle = Encoding.Unicode.GetBytes(name);
            for (int i2 = 0; i2 < sessionObj.playerInfo.UserID.Length; i2++)
            {
                if (tmpTitle.Length > i2)
                {
                    sessionObj.playerInfo.UserID[i2] = tmpTitle[i2];
                }
            }
          
            sessionObj.playerInfo.UserIdx = testUserIdx;
            sessionObj.playerInfo.RoomNo = -1;
            sc_login.Info = sessionObj.playerInfo;
            testUserIdx += 10;
            testUserNum += 1;

            LobbyInto(sessionObj);
            
            CParser.Inst.Send(sessionObj, CMarshal.Serialize(sc_login));
            SendLobbyAllRoomList(sessionObj);
            Form1.Inst.DebugMsg("로그인 보냄!");
        }

		public bool IsRoomAlreadyExist(string name) 
        {
            bool result = false;

			for (int i = 0; i < mCAllRoomList.Count; i++)
			{
				if (Encoding.Unicode.GetString( mCAllRoomList[i].info.RoomTitle ) ==  name)
				{
                    result = true;
                    break;
				}
			}


            return result;
        }
        //빈방 찾기 , 멀로 찾을까 일단 인덱스 -1인것으로
        public RoomWrapper SearchEmptyRoom()
        {
            RoomWrapper result;

            for (int i = 0; i < mCAllRoomList.Count; i++)
            {
                if (mCAllRoomList[i].info.NowUserNum <= 0)
                {
                    result = mCAllRoomList[i];
                    return result;
                }
            }

            return null;
        
        }
       
	

   

    }
}
