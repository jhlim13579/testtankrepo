﻿namespace Mycsharpsockett
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.button1 = new System.Windows.Forms.Button();
			this.textMessageBox = new System.Windows.Forms.TextBox();
			this.IpTextBox = new System.Windows.Forms.TextBox();
			this.listBox2 = new System.Windows.Forms.ListBox();
			this.listBox3 = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// listBox1
			// 
			this.listBox1.FormattingEnabled = true;
			this.listBox1.ItemHeight = 12;
			this.listBox1.Location = new System.Drawing.Point(12, 32);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(160, 256);
			this.listBox1.TabIndex = 0;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(716, 542);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// textMessageBox
			// 
			this.textMessageBox.Location = new System.Drawing.Point(313, 542);
			this.textMessageBox.Name = "textMessageBox";
			this.textMessageBox.Size = new System.Drawing.Size(397, 21);
			this.textMessageBox.TabIndex = 2;
			this.textMessageBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// IpTextBox
			// 
			this.IpTextBox.Location = new System.Drawing.Point(313, 515);
			this.IpTextBox.Name = "IpTextBox";
			this.IpTextBox.Size = new System.Drawing.Size(397, 21);
			this.IpTextBox.TabIndex = 3;
			// 
			// listBox2
			// 
			this.listBox2.FormattingEnabled = true;
			this.listBox2.HorizontalExtent = 4400;
			this.listBox2.HorizontalScrollbar = true;
			this.listBox2.ItemHeight = 12;
			this.listBox2.Location = new System.Drawing.Point(178, 32);
			this.listBox2.Name = "listBox2";
			this.listBox2.ScrollAlwaysVisible = true;
			this.listBox2.Size = new System.Drawing.Size(624, 256);
			this.listBox2.TabIndex = 4;
			this.listBox2.UseTabStops = false;
			this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
			// 
			// listBox3
			// 
			this.listBox3.FormattingEnabled = true;
			this.listBox3.HorizontalExtent = 4400;
			this.listBox3.HorizontalScrollbar = true;
			this.listBox3.ItemHeight = 12;
			this.listBox3.Location = new System.Drawing.Point(12, 294);
			this.listBox3.Name = "listBox3";
			this.listBox3.ScrollAlwaysVisible = true;
			this.listBox3.Size = new System.Drawing.Size(790, 196);
			this.listBox3.TabIndex = 5;
			this.listBox3.UseTabStops = false;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(803, 577);
			this.Controls.Add(this.listBox3);
			this.Controls.Add(this.listBox2);
			this.Controls.Add(this.IpTextBox);
			this.Controls.Add(this.textMessageBox);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.listBox1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

		#endregion

		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textMessageBox;
		private System.Windows.Forms.TextBox IpTextBox;
		public System.Windows.Forms.ListBox listBox2;
		public System.Windows.Forms.ListBox listBox3;
	}
}

