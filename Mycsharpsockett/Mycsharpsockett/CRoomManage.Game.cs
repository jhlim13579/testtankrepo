﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Mycsharpsockett
{
    public partial class CRoomManage
    {
        public void RoomLoop()
        {
            for (int i = 0; i < mCAllRoomList.Count(); i++)
            {
                if (mCAllRoomList[i].info.Gaming == true)
                {
                    for (int i2 = 0; i2 < mCAllRoomList[i].playerInfoList.Count(); i2++)
                    {
                        if (mCAllRoomList[i].playerInfoList[i2].gamerstates.userIdx > 0)
                        {
                            //탱크 정보 다 보내준다.

                            for (int i3 = 0; i3 < mCAllRoomList[i].playerInfoList.Count(); i3++)
                            {
                                sc_game_move.state[i3] = mCAllRoomList[i].playerInfoList[i3].gamerstates;
                            }

                            CParser.Inst.Send(mCAllRoomList[i].playerInfoList[i2].sessionObjRef
                             , CMarshal.Serialize(sc_game_move));

                            //솔져 정보 다 보내준다.

                            /*
                            for (int i3 = 0; i3 < mCAllRoomList[i].playerInfoList.Count(); i3++)
                            {
                                sc_game_soldier_temp.state[i3] = mCAllRoomList[i].playerInfoList[i3].gamersoldiers;
                            }

                            CParser.Inst.Send(mCAllRoomList[i].playerInfoList[i2].sessionObjRef
                               , CMarshal.Serialize(sc_game_soldier_temp));
                            */

                        }

                    }

                }
            }


        }


        public void ReceiveRoomReady(SessionObject sessionObj, CPacket.cs_room_ready msg)
        {
            if (sessionObj == null)
            {
                Form1.Inst.DebugMsg("세션 널!");
                return;
            }
            sc_room_ready.UserIdx = msg.UserIdx;
            CParser.Inst.Send(sessionObj, CMarshal.Serialize(sc_room_ready));
            SendLobbyAllRoomList(sessionObj);
            Form1.Inst.DebugMsg("게임 레디 보냄!");
        }
        public void ReceiveRoomStart(SessionObject sessionObj, CPacket.cs_room_start msg)
        {

            if (sessionObj == null)
            {
                Form1.Inst.DebugMsg("세션 널!");
                return;
            }

            //이미 게임 시작했을시 리턴
            if (mCAllRoomList[sessionObj.playerInfo.RoomNo].info.Gaming == true)
            {
                return;
            }

            mCAllRoomList[sessionObj.playerInfo.RoomNo].info.Gaming = true;
            CPacket.sc_room_start packet = sc_room_start;


            /*
            for (int i = 0; i < mCAllRoomList[sessionObj.playerInfo.RoomNo].info.MinPlayUserNum.Count(); i++)
			{
                //유저가 모자랄경우
                if (mCAllRoomList[sessionObj.playerInfo.RoomNo].info.NowPlayUserNum[i]
                    <= mCAllRoomList[sessionObj.playerInfo.RoomNo].info.MinPlayUserNum[i])
                {
                    byte[] tmpTitle = Encoding.Unicode.GetBytes("각 팀별 유저수가 부족합니다.");
                    for (int i2 = 0; i2 < packet.ErrMsg.Length; i2++)
                    {
                        if (tmpTitle.Length > i2)
                        {
                            packet.ErrMsg[i2] = tmpTitle[i2];
                        }
                    }
                    CParser.Inst.Send(sessionObj, CMarshal.Serialize(packet));
                    return;
                }
            }

            //유저가 레디를 안했을시
            for (int i = 0; i < mCAllRoomList[sessionObj.playerInfo.RoomNo].playerInfoList.Count(); i++)
            {
                if (mCAllRoomList[sessionObj.playerInfo.RoomNo].playerInfoList[i].info.isReady == true)
                {
                    byte[] tmpTitle = Encoding.Unicode.GetBytes("전체 레디 상태가 아닙니다.");
                    for (int i2 = 0; i2 < packet.ErrMsg.Length; i2++)
                    {
                        if (tmpTitle.Length > i2)
                        {
                            packet.ErrMsg[i2] = tmpTitle[i2];
                        }
                    }
                    CParser.Inst.Send(sessionObj, CMarshal.Serialize(packet));
                    return;
                }
            }
            */

            RoomWrapper tmpRoomData = null;
            for (int i = 0; i < mCAllRoomList.Count(); i++)
            {
                if (sessionObj.playerInfo.RoomNo == mCAllRoomList[i].info.RoomNo)
                {
                    tmpRoomData = mCAllRoomList[i];
                }
            }
            //3초 뒤에 시작될 리스트에 넣어준다.
            int waitTime = 0;
            Form1.TimeActWrapper tmpWrap = new Form1.TimeActWrapper();
            tmpWrap.stampTime = DateTime.Now;
            waitTime = 3000;
            if (tmpRoomData != null)
            {
                tmpWrap.act = () => Time_StartRoomReady(tmpRoomData);
                tmpWrap.actTime = waitTime;
                Form1.Inst.timeActWrappers.Add(tmpWrap);
                for (int i = 0; i < tmpRoomData.playerInfoList.Count(); i++)
                {
                    CParser.Inst.Send(tmpRoomData.playerInfoList[i].sessionObjRef, CMarshal.Serialize(packet));
                }
            }


            Form1.Inst.DebugMsg("게임 시작보냄!");
        }


        //이동처리 일단은 그냥 중계식으로 간다
        public void ReceiveMove(SessionObject sessionObj, CPacket.cs_game_move msg)
        {
            if (sessionObj == null)
            {
                Form1.Inst.DebugMsg("세션 널!");
                return;
            }
            // Form1.Inst.DebugMsg("게임 스테이트 업데이트!");
            mCAllRoomList.ElementAt(sessionObj.playerInfo.RoomNo).playerInfoList[sessionObj.playerInfo.RoomInIdx].gamerstates
                = msg.state;

        }



        //탄 맞았을시 중계해준다.
        public void ReceiveHit(SessionObject sessionObj, CPacket.cs_game_hit msg)
        {
            if (sessionObj == null)
            {
                Form1.Inst.DebugMsg("세션 널!");
                return;
            }
            // Form1.Inst.DebugMsg("게임 스테이트 업데이트!");

            for (int i = 0; i < mCAllRoomList[sessionObj.playerInfo.RoomNo].playerInfoList.Count(); i++)
            {
                sc_game_hit.hitPos = msg.hitPos;
                sc_game_hit.shotidx = msg.shotidx;
                sc_game_hit.useridx = msg.useridx;

                CParser.Inst.Send(mCAllRoomList[sessionObj.playerInfo.RoomNo].playerInfoList[i].sessionObjRef
                           , CMarshal.Serialize(sc_game_hit));
            }
        }

        //탄 주기 관리
        public void ShotLoop(PlayerWrapper player)
        {

            //보유 탄이 맞아서 해제 되었을시


            //쏠 수 있는 탄이 있는지 체크
            if (player.shotnum <= 0)
            {

            }


        }

    }
}
