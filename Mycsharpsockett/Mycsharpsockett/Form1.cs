﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using System.Collections;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;

/// <summary>
/// c# 비동기 소켓 내부적으로는 iocp
/// </summary>
namespace Mycsharpsockett
{
    public partial class Form1 : Form
    {
        public Socket mainSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
        public int maxClient = 300;
        public SpinLock mainSocketLock = new SpinLock();
        public object mainTimerLock;
        //세션 데이터 모음
        public List<SessionObject> connectedClients = new List<SessionObject>();
        public int globalTime;

        public static Form1 mInstance;
        public static Form1 Inst
        {
            get
            {
                if (null == mInstance)
                {
                    return null;
                }
                return mInstance;
			}
		}

        public class TimeActWrapper 
        {
            public Action act;
            public DateTime stampTime;
            public int actTime;
        }

        public List<TimeActWrapper> timeActWrappers = new List<TimeActWrapper>();

        public Form1()
		{
			InitializeComponent();
            mainSocketLock = new SpinLock();
            mainTimerLock = new object();
            if (null == mInstance)
			{
				mInstance = this;
			}

			try
			{
				mainSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
			}
			catch (Exception exc)
			{
			}
			IPAddress iPAddress = IPAddress.Parse("127.0.0.1");
			IPEndPoint serverEP = new IPEndPoint(iPAddress, 7001);

			mainSock.Bind(serverEP);
			mainSock.Listen(10);
            mainSock.BeginAccept(AcceptCallback, null);

            Thread thread = new Thread(() => Parsing_Thread_Method());
			thread.Start();
         
            //Thread threadTime = new Thread(() => Parsing_Thread_Time());
            //threadTime.Start();

            //샌드는 일단 워커 쓰레드에 통합시켜서 돌리는것으로
            //Thread threadSend = new Thread(() => CParser.Inst.Send_Thread_Method());
            //threadSend.Start();

        }

		//파싱을 수행하는 쓰레드의 메소드 (워커 쓰레드)
		public void Parsing_Thread_Method()
        {
			while (true)
			{
                for (int i = 0; i < connectedClients.Count(); ++i)
                {
                    SessionObject tmpObj = connectedClients[i];
                    //스핀락 진입
                    tmpObj.spinLock.Acquire();

                    //해제상태일경우
					if (tmpObj.isDead == true)
					{
                        DebugMsg("session close !" + i);
                        tmpObj.WorkingSocket.Close();
                        connectedClients.Remove(tmpObj);
                        //해제
                        tmpObj.spinLock.Release();

                    }
					else
					{
                        //파싱 & 게임 로직 실행
                        //들어온 데이터가 있으면 실행
                        if (tmpObj.Buffer_Saved_End > 0)
                        {
                            CParser.Inst.Received_Parse(tmpObj);
                        }
                        //해제
                        tmpObj.spinLock.Release();
                    }
                  
                }

                CRoomManage.Inst.RoomLoop();

                //타임 로직실행
                for (int i = 0; i < timeActWrappers.Count(); i++)
                {
                    TimeSpan tt = DateTime.Now - timeActWrappers[i].stampTime;

                    //DebugMsg("timeadd :" + Math.Abs(globalTime - timeActWrappers[i].stampTime) + " , " + timeActWrappers[i].actTime);
                    if (tt.TotalMilliseconds >= timeActWrappers[i].actTime)
                    {
                        timeActWrappers[i].act.Invoke();
                        timeActWrappers.Remove(timeActWrappers[i]);
                        DebugMsg("time invoke!");
                    }
                }

                CParser.Inst.Send_Thread_Method();

                Thread.Sleep(100);
			}
           
        }
        //100ms 기준 시간 경과시키는 쓰레드
        public void Parsing_Thread_Time()
        {
            lock (mainTimerLock) 
            {
                globalTime = 0;
            }

            while (true)
            {
				if (globalTime >= 2147483600)
				{
                    globalTime = 0;
                }
                lock (mainTimerLock) 
                {
                    globalTime += 1;
                }
                Thread.Sleep(100);
            }

        }
        void AcceptCallback(IAsyncResult ar)
        {
            mainSocketLock.Acquire();
            // 클라이언트의 연결 요청을 수락한다.
            Socket client = mainSock.EndAccept(ar);

            SessionObject obj = new SessionObject(40960, 40960);
            obj.WorkingSocket = client;
            // 연결된 클라이언트 리스트에 추가해준다.
            connectedClients.Add(obj);
            obj.num = connectedClients.Count();
            DebugMsg("session add !" + connectedClients.Count());

            client.BeginReceive(obj.Buffer, 0, obj.Buffer.Length, SocketFlags.None, Received_Async, obj);
            mainSocketLock.Release();
            mainSock.BeginAccept(AcceptCallback, null);
        }

        //비동기 리시브 여기서 클라이언트의 패킷을 IOCP로 비동기적으로 받는다 
        //받을때  SessionObject 로 세션데이터와 함께 받음
        public void Received_Async(IAsyncResult ar)
        {
            SessionObject obj = (SessionObject)ar.AsyncState;
            SocketError socketError;

            if (obj.isDead == true)
            {
                return;
            }

            obj.spinLock.Acquire();


            try
            {
                if (obj.WorkingSocket.Connected)
                {
                    int dataRead = obj.WorkingSocket.EndReceive(ar, out socketError);

                    if (socketError != SocketError.Success)
                    {
                        obj.isDead = true;
                    }
                    if (dataRead <= 0)
                    {
                        obj.isDead = true;
                    }
                    //받은 만큼 저장버퍼 끝지점 에 복사
                    Array.Copy(obj.Buffer, 0, obj.Buffer_Saved, obj.Buffer_Saved_End, dataRead);
                    obj.Buffer_Saved_End += dataRead;
                  //  Invoke((MethodInvoker)delegate
                  //  {
                        //listBox3.Items.Add(string.Format("Receive Msg:{0}", dataRead));
                 //   });
                    // 데이터를 받은 후엔 다시 버퍼를 비워주고 같은 방법으로 수신을 대기한다.
                    //obj.ClearBuffer();

                    // 수신 대기
                    // AcceptCallback 함수에서의 client와 obj.WorkingSocket은 동일한 소켓 개체이다!
                    obj.WorkingSocket.BeginReceive(obj.Buffer, 0, obj.Buffer.Length, SocketFlags.None, Received_Async, obj);
                }

            }
            catch (Exception exc)
            {
                obj.isDead = true;
            }
            obj.spinLock.Release();

        }

        public void DebugMsg(string str)
        {
            //MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            try
            {
                Invoke((MethodInvoker)delegate
                {
                    listBox3.Items.Add(string.Format("Msg:{0}", str));
                });
            }
            catch { }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    
        private void button1_Click(object sender, EventArgs e)
        {

        }

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

        //리시브 메인 여기서 분기처리
        public void RecieveData(SessionObject sessionObj, byte[] msg )
        {
            //try
            {
               // DebugMsg(string.Format("Parse : {0}" , sessionObj.header.PacketId));
                
                switch (sessionObj.header.PacketId)
                {

                    case (UInt16)CPacket.PACKET_INFO.cs_login:
                        CRoomManage.Inst.ReceiveLogin(sessionObj, CMarshal.Deserialize<CPacket.cs_login>(msg));
                        break;

                    case (UInt16)CPacket.PACKET_INFO.cs_room_make:
                        CRoomManage.Inst.ReceiveLobbyRoomMake(sessionObj, CMarshal.Deserialize<CPacket.cs_room_make>(msg));
                        break;

                    case (UInt16)CPacket.PACKET_INFO.cs_room_into:
                        CRoomManage.Inst.ReceiveLobbyRoomInto(sessionObj, CMarshal.Deserialize<CPacket.cs_room_into>(msg));
                        break;

                    case (UInt16)CPacket.PACKET_INFO.cs_room_out:
                        CRoomManage.Inst.ReceiveLobbyRoomOut(sessionObj, CMarshal.Deserialize<CPacket.cs_room_out>(msg));
                        break;

                    case (UInt16)CPacket.PACKET_INFO.cs_room_ready:
                        CRoomManage.Inst.ReceiveRoomReady(sessionObj, CMarshal.Deserialize<CPacket.cs_room_ready>(msg));
                        break;

                    case (UInt16)CPacket.PACKET_INFO.cs_room_start:
                        CRoomManage.Inst.ReceiveRoomStart(sessionObj, CMarshal.Deserialize<CPacket.cs_room_start>(msg));
                        break;

                    case (UInt16)CPacket.PACKET_INFO.cs_game_move:
                        CRoomManage.Inst.ReceiveMove(sessionObj, CMarshal.Deserialize<CPacket.cs_game_move>(msg));
                        break;

                    case (UInt16)CPacket.PACKET_INFO.cs_game_hit:
                        CRoomManage.Inst.ReceiveHit(sessionObj, CMarshal.Deserialize<CPacket.cs_game_hit>(msg));
                        break;

                    default:
                        DebugMsg("no match packet !");
                        break;
                }
                
            }
           // catch (Exception exc) 
            {
           //     DebugMsg(exc.ToString());
            }


        }
    }
}
