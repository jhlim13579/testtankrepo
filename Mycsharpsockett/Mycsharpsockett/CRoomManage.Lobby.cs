﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Mycsharpsockett
{
    public partial class CRoomManage 
    {
		//로비 진입
		public void LobbyInto(SessionObject sessionObj) 
        {

            mCAllLobbyUserList.Add(new PlayerWrapper
            {
                info = sessionObj.playerInfo ,
                sessionObjRef = sessionObj
            });


        }

        //로비 나감
        public void LobbyOut(SessionObject sessionObj)
        {

			for (int i = 0; i < mCAllLobbyUserList.Count(); i++)
			{
				if (mCAllLobbyUserList[i].info.UserIdx == sessionObj.playerInfo.UserIdx)
				{
                    mCAllLobbyUserList.RemoveAt(i);
                    break;
				}
			}


        }

        //대기상태에서 시간 지나면 자동 게임 시작
        public void Time_StartRoomReady(RoomWrapper roomWrapper)
        {

            //Todo : 나중에 이 부분 게임 분산 서버로 넘겨야함 , 로비서버와 게임서버의 분리
            //3초 대기하는동안 게임서버에 데이터 넘겨주고 게임 시작


            //방 전체 유저에게 게임 시작 알림
            for (int i = 0; i < roomWrapper.playerInfoList.Count(); i++)
            {
                SessionObject sessionObj = roomWrapper.playerInfoList[i].sessionObjRef;

                sessionObj.playerInfo.UserID = new byte[32];
                byte[] tmpTitle = Encoding.Unicode.GetBytes("testIdName");
                for (int i2 = 0; i2 < sessionObj.playerInfo.UserID.Length; i2++)
                {
                    if (tmpTitle.Length > i2)
                    {
                        sessionObj.playerInfo.UserID[i2] = tmpTitle[i2];
                    }
                }

                sc_game_init.roomData = roomWrapper.info;
               
                for (int i2 = 0; i2 < roomWrapper.playerInfoList.Count(); i2++)
                {
                    sc_game_init.UserData[i2] = roomWrapper.playerInfoList[i2].info;
                }
                testUserNum += 1;
                CParser.Inst.Send(sessionObj, CMarshal.Serialize(sc_game_init));
                Form1.Inst.DebugMsg("게임 진입 보냄!");


            }

        }

        //룸에 유저가 진입하려는 명령 보냈을시
        public void ReceiveLobbyRoomInto(SessionObject sessionObj, CPacket.cs_room_into msg)
        {
            if (sessionObj == null)
            {
                Form1.Inst.DebugMsg("세션 널!");
                return;
            }
            try
            {
                CPacket.roominfo info = mCAllRoomList.ElementAt(msg.RoomNo).info;

            
                //룸이 존재하는지
                if (info.RoomNo < 0)
                {
                    sc_room_into.ErrMsg = new byte[32];
                    byte[] tmpTitle = Encoding.Unicode.GetBytes("해당 방이 존재하지 않습니다.");
                    for (int i2 = 0; i2 < sc_room_into.ErrMsg.Length; i2++)
                    {
                        if (tmpTitle.Length > i2)
                        {
                            sc_room_into.ErrMsg[i2] = tmpTitle[i2];
                        }
                    }

                    CParser.Inst.Send(sessionObj, CMarshal.Serialize(sc_room_into));
                    return;
                }

                //룸이 꽉차있는지
                if (info.NowUserNum >= info.MaxUserNum)
                {
                    sc_room_into.ErrMsg = new byte[32];
                    byte[] tmpTitle = Encoding.Unicode.GetBytes("방 최대 인원수를 초과하였습니다.");
                    for (int i2 = 0; i2 < sc_room_into.ErrMsg.Length; i2++)
                    {
                        if (tmpTitle.Length > i2)
                        {
                            sc_room_into.ErrMsg[i2] = tmpTitle[i2];
                        }
                    }

                    CParser.Inst.Send(sessionObj, CMarshal.Serialize(sc_room_into));

                    return;
                }

                //룸이 게임 중인지
                if (info.Gaming == true)
                {
                    sc_room_into.ErrMsg = new byte[32];
                    byte[] tmpTitle = Encoding.Unicode.GetBytes("해당 방이 게임 시작 상태입니다.");
                    for (int i2 = 0; i2 < sc_room_into.ErrMsg.Length; i2++)
                    {
                        if (tmpTitle.Length > i2)
                        {
                            sc_room_into.ErrMsg[i2] = tmpTitle[i2];
                        }
                    }

                    CParser.Inst.Send(sessionObj, CMarshal.Serialize(sc_room_into));
                    return;
                }

                sessionObj.playerInfo.RoomInIdx = mCAllRoomList.ElementAt(msg.RoomNo).playerInfoList.Count();
                sessionObj.playerInfo.RoomNo = info.RoomNo;
                //해당 유저 룸내 밴 상태 아닐시 후순
                //룸 안의 데이터에 해당 유저 데이터 추가해준다.
                CPacket.playerinfo tmpInfo = sessionObj.playerInfo;
                mCAllRoomList.ElementAt(msg.RoomNo).playerInfoList.Add(new PlayerWrapper { info = tmpInfo, sessionObjRef = sessionObj });
                mCAllRoomList.ElementAt(msg.RoomNo).info.NowUserNum += 1;


                //해당 유저의 룸 번호 업데이트
                sessionObj.playerInfo.RoomNo = info.RoomNo;
                for (int i = 0; i < mCAllRoomList.ElementAt(msg.RoomNo).playerInfoList.Count(); i++)
                {
                    sc_room_into.playerinfos[i] = mCAllRoomList.ElementAt(msg.RoomNo).playerInfoList[i].info;
                }

                //룸내 유저들에게 진입패킷 쏴준다.
                for (int i = 0; i < mCAllRoomList.ElementAt(msg.RoomNo).playerInfoList.Count(); i++)
                {
                    Form1.Inst.DebugMsg("해당 유저 진입 메시지!" + i);
                    SessionObject tmpSessionObj = mCAllRoomList.ElementAt(msg.RoomNo).playerInfoList[i].sessionObjRef;
                    CParser.Inst.Send(tmpSessionObj, CMarshal.Serialize(sc_room_into));
                }

            }
            catch (Exception exc)
            {
                Form1.Inst.DebugMsg(exc.ToString());
            }

        }

        //룸 아웃 요청
        public void ReceiveLobbyRoomOut(SessionObject sessionObj, CPacket.cs_room_out msg)
        {

            //유저 해당 룸에 존재하는지
            if (sessionObj.playerInfo.RoomNo != msg.RoomNo)
            {
                Form1.Inst.DebugMsg("해당 룸에 존재하지않음!");
                return;
            }

            //룸내 유저들에게 아웃패킷 쏴준다.
            if (sessionObj == null)
            {
                Form1.Inst.DebugMsg("세션 널!");
                return;
            }
            try
            {
                //해당 유저 룸 넘버 -1로 초기화
                sessionObj.playerInfo.RoomNo = -1;
                //해당 룸의 유저 데이터 없애준다.
                for (int i = 0; i < mCAllRoomList.Count(); i++)
                {
                    if (mCAllRoomList[i].info.RoomNo == msg.RoomNo)
                    {
                        if (mCAllRoomList[i].info.NowUserNum >= 1)
                        {
                            mCAllRoomList[i].info.NowUserNum -= 1;
                        }
                    }
                    //Todo : nowplayuser의 useridx 도 구분하여 없애줄까 생각중임

                }

                //룸 아웃 성공 메시지 전송
                sc_room_out.UserIdx = sessionObj.playerInfo.UserIdx;
                OutProcess(sessionObj);
                CParser.Inst.Send(sessionObj, CMarshal.Serialize(sc_room_out));
                //로비 룸 인포 다시 보내줌
                SendLobbyAllRoomList(sessionObj);

            }
            catch (Exception exc)
            {
                Form1.Inst.DebugMsg(exc.ToString());
            }
        }


        //룸 생성 요청
        public void ReceiveLobbyRoomMake(SessionObject sessionObj, CPacket.cs_room_make msg)
        {
            if (sessionObj == null)
            {
                Form1.Inst.DebugMsg("세션 널!");
                return;
            }
            try
            {
                //이름 충복 확인
                //	if (IsRoomAlreadyExist(msg.RoomTitle) == true)
                {

                }
                //필터확인 후순

                //빈 방이 있을시
                if (SearchEmptyRoom() != null)
                {
                    //룸 리스트를 순회하며 비어있는 룸에 할당
                    RoomWrapper tmpRoom = SearchEmptyRoom();
                    tmpRoom.playerInfoList.Add(new PlayerWrapper { sessionObjRef = sessionObj, info = sessionObj.playerInfo });
                    tmpRoom.playerInfoList.Last().gamerstates.userIdx = sessionObj.playerInfo.UserIdx;
                    //룸넘버 업데이트
                    sessionObj.playerInfo.RoomNo = tmpRoom.info.RoomNo;

                    //방의 모든 유저에게 샌드
                    for (int i = 0; i < tmpRoom.playerInfoList.Count(); i++)
                    {
                        sc_room_into.Info = tmpRoom.info;
                        CParser.Inst.Send(sessionObj, CMarshal.Serialize(sc_room_into));
                        Form1.Inst.DebugMsg("send room make to user" + i);
                    }

                    SendLobbyAllRoomListAllUser();
                }
                //빈 방 없음
                else
                {

                }


            }
            catch (Exception exc)
            {
                Form1.Inst.DebugMsg(exc.ToString());
            }
        }

        private void SendLobbyAllRoomList(SessionObject sessionObj)
        {
            //보낼 데이터 : 룸의 모든 방에 대한 데이터 보내줘야한다.
            // try
            {
                if (sessionObj != null)
                {
                    if (sessionObj.WorkingSocket != null)
                    {
                        //연결 되있는지 로비에 있는지 체크
                        if (sessionObj.WorkingSocket.Connected && sessionObj.playerInfo.RoomNo < 0)
                        {
                            int curl = 0;
                            while (curl < mCAllRoomList.Count)
                            {
                                sc_roomlist_info.CurInfo = curl;

                                //10개씩 스플릿해서 수량만큼 쪼개서
                                for (int i = 0; i < 10; i++)
                                {
                                    if (mCAllRoomList.Count > curl + i)
                                    {
                                        sc_roomlist_info.Info[i] = mCAllRoomList[curl + i].info;

                                    }
                                }
                                //if (mCAllRoomList.Count >= curl )
                                {
                                    CParser.Inst.Send(sessionObj, CMarshal.Serialize(sc_roomlist_info));
                                    curl += 10;
                                }

                                Form1.Inst.DebugMsg("컬 인포 :" + curl.ToString());
                            }
                        }
                    }
                }
            }
            //   catch (Exception exc)
            //   {
            //        Form1.Inst.DebugMsg(exc.Message);
            //   }
        }

        //로비에 있는 모든 유저에게 로비 방 정보 재전달
        private void SendLobbyAllRoomListAllUser()
        {
			for (int i = 0; i < mCAllLobbyUserList.Count; i++)
			{
                SessionObject sessionObj = mCAllLobbyUserList[i].sessionObjRef;
                //보낼 데이터 : 룸의 모든 방에 대한 데이터 보내줘야한다.
                try
                {
                    if (sessionObj != null)
                    {
                        if (sessionObj.WorkingSocket != null)
                        {
                            //연결 되있는지 로비에 있는지 체크
                            if (sessionObj.WorkingSocket.Connected && sessionObj.playerInfo.RoomNo < 0)
                            {
                                int curl = 0;
                                while (curl < mCAllRoomList.Count)
                                {
                                    sc_roomlist_info.CurInfo = curl;

                                    //10개씩 스플릿해서 수량만큼 쪼개서
                                    for (int i2 = 0; i2 < 10; i2++)
                                    {
                                        if (mCAllRoomList.Count > curl + i2)
                                        {
                                            sc_roomlist_info.Info[i2] = mCAllRoomList[curl + i2].info;
                                        }
                                    }
                                    //if (mCAllRoomList.Count >= curl )
                                    {
                                        CParser.Inst.Send(sessionObj, CMarshal.Serialize(sc_roomlist_info));
                                        curl += 10;
                                    }

                                    Form1.Inst.DebugMsg("컬 인포 :" + curl.ToString());
                                }
                            }
                        }
                    }
                }
                catch (Exception exc)
                {
                     Form1.Inst.DebugMsg(exc.Message);
                }
            }

        }


    }
}
