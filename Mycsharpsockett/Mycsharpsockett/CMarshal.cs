﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

namespace Mycsharpsockett
{
    public static class CMarshal
    {
        public static int GetSize(object _object)
        {
            return Marshal.SizeOf(_object);
        }
        public struct TypeSizeProxy<T>
        {
            public T PublicField;
        }
        public static int GetSize <T>()
        {
            try
            {
                return Marshal.SizeOf(typeof(T));
            }
            catch (ArgumentException)
            {
                return Marshal.SizeOf(new TypeSizeProxy<T>());
            }
        }
        //패킷 구조체를 바이트로 변환
        public static byte[] Serialize (object _object)
        {
            byte[] searializeBuffer = new byte[Marshal.SizeOf(_object)];

            unsafe
            {
                fixed (byte* fixed_Buffer = searializeBuffer)
                    Marshal.StructureToPtr(_object, (IntPtr)fixed_Buffer, false);
            }

            return searializeBuffer;


        }
        //바이트배열을 패킷 구조체로 변환
        public static T Deserialize <T>( byte [] _byte , int offset = 0)
        {
            int objectSize = GetSize<T>();

            if (objectSize > _byte.Length)
            {
                return default;
            }
            IntPtr buffer = Marshal.AllocHGlobal(objectSize);
            Marshal.Copy(_byte, offset, buffer, objectSize);
            object returnobj = Marshal.PtrToStructure<T>(buffer);
            Marshal.FreeHGlobal(buffer);
            return (T)returnobj;

        }


    }
}
