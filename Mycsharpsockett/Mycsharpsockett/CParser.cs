﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using System.Collections;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;


namespace Mycsharpsockett
{
    public class SpinLock
    {
        volatile int locked = 0; 
        public void Acquire()
        {
            while (true)
            {
                int original_value = Interlocked.Exchange(ref locked, 1);
                if (original_value == 0) //만약 원래 값이 0이라면 다른 스레드가 사용하지 않았다고 판단.
                    break; 
            } 
          
        } 
        public void Release() 
        { 
            locked = 0; 
        } 
    }

    public class CParser
    {

    
        public class SendStr 
        {
            public byte[] data;
            public SessionObject sessionObj;
        }
        public Queue<SendStr> mSendQueue = new Queue<SendStr>();

        public static CParser mInstance;
		public static CParser Inst
		{
			get
			{
				if (null == mInstance)
				{
                    mInstance = new CParser();
                    return mInstance;
                }
				return mInstance;
			}
         
		}
        public CParser() 
        {
            if (null == mInstance)
            {
                mInstance = this;
            }
        }

		public void Send(SessionObject sessionObj, byte[] data)
        {
            mSendQueue.Enqueue(new SendStr { sessionObj = sessionObj, data = data });
        }


        public void Send_Thread_Method()
        {
            for (int i = 0; i < mSendQueue.Count(); i++)
            {
                SpinLock lockobj = mSendQueue.Peek().sessionObj.spinLock;
                lockobj.Acquire();

                byte[] sendData = mSendQueue.Peek().data;

                if (mSendQueue.Peek().sessionObj.isDead == true)
                {
                    mSendQueue.Dequeue();
                }
                else
                {
                    try
                    {
                        mSendQueue.Peek().sessionObj.WorkingSocket.Send(sendData, sendData.Length, SocketFlags.None);
                        mSendQueue.Dequeue();
                    }
                    catch
                    {
                        mSendQueue.Dequeue();
                        Form1.Inst.DebugMsg("샌드 실패 이미 해제된 소켓!");
                    }
                }
                lockobj.Release();
            }
        }

        //파싱해주는 메서드 파싱 쓰레드에서 전체 세션을 순회하며 수행한다.
        public void Received_Parse(SessionObject obj)
        {
            //해더 읽기 가능한 사이즈인지
            if (obj.Buffer_Saved_End >= 5) { }
            else { return; }

            //해더 처음 읽었을시
            if (obj.header_read == false)
            {
                byte[] myread = new byte[5];
                Array.Copy(obj.Buffer_Saved, 0, myread, 0, 5);
                obj.header = CMarshal.Deserialize<CPacket.PACKET_HEADER>(myread);
                obj.header_read = true;
            }

            if (obj.header_read == true)
            {
                //헤더 보다 작은지
                if (obj.header.PacketLength > obj.Buffer_Saved_End)
                {
                    //작다면 아무것도 하지 않고 뒤의 패킷을 대기한다.
                }
                //헤더 보다 큰지
                //크다면 헤더만큼 복사해서 워커 쓰레드 기능 실행 오버된 나머지는 남겨서 뒤에 오는 패킷이랑 합쳐서
                //다시 재귀적으로 Received_Parse 실행
                else if (obj.header.PacketLength < obj.Buffer_Saved_End)
                {
                    //해더만큼의 복사하고 워커 쓰레드에 해당 패킷 기능 맡긴다.
                    byte[] tempsaved = new byte[obj.header.PacketLength];
                    Array.Copy(obj.Buffer_Saved, 0, tempsaved, 0, obj.header.PacketLength);
                    Form1.Inst.RecieveData(obj, tempsaved);

                    //오버된 만큼의 복사
                    int oversize = obj.Buffer_Saved_End - obj.header.PacketLength;
                    byte[] oversaved = new byte[oversize];
                    Array.Copy(obj.Buffer_Saved, obj.header.PacketLength, oversaved, 0, oversize);
                    Array.Copy(oversaved, 0, obj.Buffer_Saved, 0, oversize);
                    obj.Buffer_Saved_End -= obj.header.PacketLength;
                    obj.header_read = false;
                    Received_Parse(obj);
                }
                //헤더와 동일하게 일치한다면
                else if (obj.header.PacketLength == obj.Buffer_Saved_End)
                {
                    //해더만큼의 복사하고 워커 쓰레드에 해당 패킷 기능 맡긴다.
                    byte[] tempsaved = new byte[obj.header.PacketLength];
                    Array.Copy(obj.Buffer_Saved, 0, tempsaved, 0, obj.header.PacketLength);
                    obj.Buffer_Saved_End -= obj.header.PacketLength;
                    obj.header_read = false;
                    Form1.Inst.RecieveData(obj, tempsaved);
                }
            }

        }


    }



}
