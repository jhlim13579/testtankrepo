﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Networking;

using Aws.GameLift.Server;
using Aws.GameLift.Server.Model;

namespace CSampleServer
{
	using GameServer;

	/// <summary>
	/// 하나의 session객체를 나타낸다.
	/// </summary>
	class CGameUser : IPeer
	{
        private readonly UserToken _token;
        private readonly Action<Packet> _onMessage;

        //공통 데이터 처리용 참조가 필요하다.
        SharedData dataShareds;

		public CGameUser(UserToken token, object sharedDataObj, Action<Packet> onMessage = null)
        {
            dataShareds = (SharedData)sharedDataObj;
            _token = token;
            _token.SetPeer(this);
            token.closeAction = ()=> Program.remove_user(this);
            if (onMessage == null)
            {
                _onMessage = MessageMethod;
            }
            else
            {
                _onMessage = onMessage;
            }
        }


        public void Disconnect()
        {
            throw new NotImplementedException();
        }

        public void OnMessage(Packet msg)
        {
            _onMessage(msg);
        }

        public void OnRemoved()
        {
          
            throw new NotImplementedException();
        }

        
        public void Send(Packet msg)
        {
            
            _token.Send(msg);
        }
        
   

		void MessageMethod(Packet msg)
		{
            // 에코서버 테스트할 때 사용함.
            // Remove below comments to use echo server.
            //send(msg);
            //return;

            // ex)
            PROTOCOL protocol = (PROTOCOL)msg.PopProtocolID();
            //Console.WriteLine("------------------------------------------------------");
            //Console.WriteLine("protocol id " + protocol);
            switch (protocol)
            {
                case PROTOCOL.cs_login:
                    {

                    }
                    break;



                case PROTOCOL.CHAT_MSG_REQ:
                    {

                        
                        Proto.cs_login tmp = msg.PopObj<Proto.cs_login>();


                        string text = tmp.UserPwd;
                        dataShareds.allPacketNum += 1;
                        //Console.WriteLine(string.Format("num {0}", dataShareds.allPacketNum));
					
                        Packet response = Packet.Create((short)PROTOCOL.CHAT_MSG_ACK);
                       
                        //response.Owner = _token;
                        //response.push(text);
                        Proto.cs_login tmp2 = new Proto.cs_login();
                        tmp2.UserPwd = "aqqqqsdfasdf";

                       // /*
                        DateTime nowTime = DateTime.Now;
                        Proto.customTime myTime = new Proto.customTime()
                        {
                            year = nowTime.Year,
                            month = nowTime.Month,
                            day = nowTime.Day,
                            hour = nowTime.Hour,
                            minute = nowTime.Minute,
                            second = nowTime.Second,
                            millisecond = nowTime.Millisecond,
                        };
                        tmp2.allPackNum = dataShareds.allPacketNum;
                        tmp2.timeTickS = myTime;
                        tmp2.timeTickC = tmp.timeTickC;
                      //  */

                        /*
                        DateTime nowTime = DateTime.Now;
                        //처리시간 계산
                        Proto.customTime myTime = new Proto.customTime()
                        {
                            year = nowTime.Year,
                            month = nowTime.Month,
                            day = nowTime.Day,
                            hour = nowTime.Hour,
                            minute = nowTime.Minute,
                            second = nowTime.Second,
                            millisecond = nowTime.Millisecond,
                        };
                        tmp2.allPackNum = dataShareds.allPacketNum;
                        tmp2.timeTickS = myTime;
                        tmp2.timeTickC = tmp.timeTickC;
                        */

                        
                      //  string mytex = msg.PopString();

                        // Console.WriteLine(mytex);

                        //Packet msg2 = Packet.Create((short)PROTOCOL.CHAT_MSG_ACK);
                        //msg2.Push("testtest");
                        //Send(msg2);
                        
                        response.Push(tmp2);
                        Send(response);


                        /*
                        if (text.Equals("exit"))
                        {
                            // 대량의 메시지를 한꺼번에 보낸 후 종료하는 시나리오 테스트.
                            for (int i = 0; i < 1000; ++i)
                            {
                                CPacket dummy = CPacket.create((short)PROTOCOL.CHAT_MSG_ACK);
                                dummy.push(i.ToString());
                                send(dummy);
                            }

                            this.token.ban();
                        }
                        */
                    }
                    break;
            }
        }
	}
}
