﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Networking;

using Aws.GameLift;
using Aws.GameLift.Server;
using Aws.GameLift.Server.Model;

namespace CSampleServer
{
	class Program
	{
		static List<CGameUser> userlist;
        
		public SharedData sharedData;

     
        int listeningPort = 7777;
         bool IsAlive = false;

        static Program instance;
        public static Program Instance() 
        {
			if (instance == null)
			{
                instance = new Program();
            }
            return instance;
        }
        public static void Main(string[] args)
        {

            instance = new Program();
            //instance. AwsStart();
            instance.LaunchServer();
        }
        public  void AwsStart() 
        {
			var initSDKOutcome = GameLiftServerAPI.InitSDK();
            if (initSDKOutcome.Success)
            {
                ProcessParameters processParameters = new ProcessParameters(
                    this.OnStartGameSession,
                    this.OnUpdateGameSession,
                    this.OnProcessTerminate,
                    this.OnHealthCheck,
                    listeningPort, //This game server tells GameLift that it will listen on port 7777 for incoming player connections.
                    new LogParameters(new List<string>()
                    {
                    //Here, the game server tells GameLift what set of files to upload when the game session ends.
                    //GameLift will upload everything specified here for the developers to fetch later.
                    "/local/game/logs/myserver.log"
                    }));

                //Calling ProcessReady tells GameLift this game server is ready to receive incoming game sessions!
                var processReadyOutcome = GameLiftServerAPI.ProcessReady(processParameters);
                if (processReadyOutcome.Success)
                {
                    // Set Server to alive when ProcessReady() returns success
                    IsAlive = true;

                    // Create a TCP listener(in a listener thread) from port when when ProcessReady() returns success
                    LaunchServer();

                    Console.WriteLine("ProcessReady success.");
                }
                else
                {
                    IsAlive = false;
                    Console.WriteLine("ProcessReady failure : " + processReadyOutcome.Error.ToString());
                }
            }
            else
            {
                IsAlive = true;
                Console.WriteLine("InitSDK failure : " + initSDKOutcome.Error.ToString());
            }

        }
        void OnStartGameSession(GameSession gameSession)
        {
            //When a game session is created, GameLift sends an activation request to the game server and passes along the game session object containing game properties and other settings.
            //Here is where a game server should take action based on the game session object.
            //Once the game server is ready to receive incoming player connections, it should invoke GameLiftServerAPI.ActivateGameSession()
            Console.WriteLine($"Server : OnStartGameSession() called");
            GameLiftServerAPI.ActivateGameSession();
        }

        void OnUpdateGameSession(UpdateGameSession updateGameSession)
        {
            //When a game session is updated (e.g. by FlexMatch backfill), GameLiftsends a request to the game
            //server containing the updated game session object.  The game server can then examine the provided
            //matchmakerData and handle new incoming players appropriately.
            //updateReason is the reason this update is being supplied.
            Console.WriteLine($"Server : OnUpdateGameSession() called");
        }

        void OnProcessTerminate()
        {
            //OnProcessTerminate callback. GameLift will invoke this callback before shutting down an instance hosting this game server.
            //It gives this game server a chance to save its state, communicate with services, etc., before being shut down.
            //In this case, we simply tell GameLift we are indeed going to shutdown.
            Console.WriteLine($"Server : OnProcessTerminate() called");
            GameLiftServerAPI.ProcessEnding();
        }

        bool OnHealthCheck()
        {
            //This is the HealthCheck callback.
            //GameLift will invoke this callback every 60 seconds or so.
            //Here, a game server might want to check the health of dependencies and such.
            //Simply return true if healthy, false otherwise.
            //The game server has 60 seconds to respond with its health status. GameLift will default to 'false' if the game server doesn't respond in time.
            //In this case, we're always healthy!
            Console.WriteLine($"Server : OnHealthCheck() called");
            return true;
        }
        public void LaunchServer() 
        {
            userlist = new List<CGameUser>();
            sharedData = new SharedData();
            NetworkService service = new NetworkService(true);
            // 콜백 매소드 설정.
            service.SessionCreatedCallback += on_session_created;
            // 초기화.
            service.Initialize(3000 , 2000);
            service.Listen("0.0.0.0", 7979, 10);


            Console.WriteLine("Started!");
            while (true)
            {
                //Console.Write(".");
                string input = Console.ReadLine();
                if (input.Equals("users"))
                {
                    Console.WriteLine(service.UserManager.GetTotalCount());
                }
                System.Threading.Thread.Sleep(1000);
            }
            service.EndListen();
            //Console.ReadKey();
        }

      

		/// <summary>
		/// 클라이언트가 접속 완료 하였을 때 호출됩니다.
		/// n개의 워커 스레드에서 호출될 수 있으므로 공유 자원 접근시 동기화 처리를 해줘야 합니다.
		/// </summary>
		/// <returns></returns>
		static void on_session_created(UserToken token)
		{
            lock (userlist)
            {
				CGameUser user = new CGameUser(token , instance.sharedData);
                userlist.Add(user);
				Console.WriteLine("Connected!" + userlist.Count);
            }
		}

		public static void remove_user(CGameUser user)
		{
            lock (userlist)
            {
                userlist.Remove(user);
			Console.WriteLine("DisConnected! " + userlist.Count);
            }
		}







        public GenericOutcome AcceptPlayerSession(string playerSessionId)
        {
            return GameLiftServerAPI.AcceptPlayerSession(playerSessionId);
        }

        public void RemovePlayerSession(string playerSessionId)
        {
            Console.WriteLine("RemovePlayerSession for player session id: " + playerSessionId);

            try
            {
                // Remove players from the game session that disconnected
                var outcome = GameLiftServerAPI.RemovePlayerSession(playerSessionId);

                if (outcome.Success)
                {
                    Console.WriteLine("PLAYER SESSION REMOVED");
                }
                else
                {
                    Console.WriteLine("PLAYER SESSION REMOVE FAILED. RemovePlayerSession() returned " + outcome.Error.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("PLAYER SESSION REMOVE FAILED. RemovePlayerSession() exception " + Environment.NewLine + e.Message);
                throw;
            }
        }
        private void FinalizeServerProcessShutdown()
        {
            Console.WriteLine("GameLiftServer.FinalizeServerProcessShutdown");

            // All game session clean up should be performed before this, as it should be the last thing that
            // is called when terminating a game session. After a successful outcome from ProcessEnding, make 
            // sure to call Application.Quit(), otherwise the application does not shutdown properly. see:
            // https://forums.awsgametech.com/t/server-process-exited-without-calling-processending/5762/17

            var outcome = GameLiftServerAPI.ProcessEnding();
            if (outcome.Success)
            {
                Console.WriteLine("FinalizeServerProcessShutdown: GAME SESSION TERMINATED");
                //Application.Quit();
            }
            else
            {
                Console.WriteLine("FinalizeServerProcessShutdown: GAME SESSION TERMINATION FAILED. ProcessEnding() returned " + outcome.Error.ToString());
            }
        }

        public void HandleGameEnd()
        {
            Console.WriteLine("HandleGameEnd");

            FinalizeServerProcessShutdown();
        }

        // a Unity callback when the program is quitting
        void OnApplicationQuit()
        {
            Console.WriteLine("GameLiftServer.OnApplicationQuit");

            FinalizeServerProcessShutdown();
        }
    }
}
