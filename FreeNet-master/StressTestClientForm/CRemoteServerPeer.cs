﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Networking;

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using System.Drawing.Design;


namespace StressTestClientForm
{
	public class MultiLineTextEditor : UITypeEditor
	{
		private IWindowsFormsEditorService _editorService;

		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.DropDown;
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			_editorService = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

			TextBox textEditorBox = new TextBox();
			textEditorBox.Multiline = true;
			textEditorBox.ScrollBars = ScrollBars.Vertical;
			textEditorBox.Width = 250;
			textEditorBox.Height = 150;
			textEditorBox.BorderStyle = BorderStyle.None;
			textEditorBox.AcceptsReturn = true;
			textEditorBox.Text = value as string;

			_editorService.DropDownControl(textEditorBox);

			return textEditorBox.Text;
		}
	}

	public class CRemoteServerPeer : IPeer
	{
		private readonly UserToken _token;
		private readonly Action<Packet> _onMessage;

		public object UserLock;

		[Category("Datas")]
		[Description("alltex")]
		[Editor(typeof(MultiLineTextEditor), typeof(UITypeEditor))]
		public string allTex { get; set; }

		[Category("Datas")]
		[Description("chkNum")]
		public int msgChkNum { get; set; }

		[Category("Datas")]
		[Description("chkNums")]
		public double[] msgChkNums { get; set; }
		
		public StressSharedData sharedData;

		
		public CRemoteServerPeer(UserToken token, object sharedDataObj, Action<Packet> onMessage = null)
		{
			UserLock = new object();
			sharedData = (StressSharedData)sharedDataObj;
			sharedData.allUserNum += 1;
			msgChkNums = new double[500];

			_token = token;
			_token.SetPeer(this);
			if (onMessage == null)
			{
				_onMessage = MessageMethod;
			}
			else
			{
				_onMessage = onMessage;
			}
		}

        int recv_count = 0;
		public void OnMessage(Packet msg)
		{
			_onMessage(msg);
		}
		public void MessageMethod(Packet msg)
		{
			lock (UserLock)
			{


				System.Threading.Interlocked.Increment(ref this.recv_count);

				PROTOCOL protocol_id = (PROTOCOL)msg.PopProtocolID();
				switch (protocol_id)
				{
					case PROTOCOL.CHAT_MSG_ACK:
						{

							/*
							string text = msg.PopString();
							allTex += text;
							if (allTex.Length > 500)
							{
								lock (Form1.Instance.msgList)
								{
									Form1.Instance.msgList.Enqueue(string.Format(allTex + " ;"));
								}
								allTex = "";
							}
							*/



							Proto.cs_login tmp = msg.PopObj<Proto.cs_login>();
							string text = tmp.UserPwd;
							allTex += text;
							if (allTex.Length > 500)
							{
								lock (Form1.Instance.msgList)
								{
									Form1.Instance.msgList.Enqueue(string.Format(allTex + " ;"));
								}
								allTex = "";
							}

							//처리시간 계산
							DateTime sendTime = new DateTime(
								 tmp.timeTickC.year,
								 tmp.timeTickC.month,
								 tmp.timeTickC.day,
								 tmp.timeTickC.hour,
								 tmp.timeTickC.minute,
								 tmp.timeTickC.second,
								 tmp.timeTickC.millisecond
								 );
							DateTime recieveTime = new DateTime(
								 tmp.timeTickS.year,
								 tmp.timeTickS.month,
								 tmp.timeTickS.day,
								 tmp.timeTickS.hour,
								 tmp.timeTickS.minute,
								 tmp.timeTickS.second,
								 tmp.timeTickS.millisecond
								 );
							//시간의 변화량 - 클라가 받았을때의 시간 - 서버가 보냈을때의 시간 
							TimeSpan timeDiffSpan = DateTime.Now - sendTime;
							double timeDiff = timeDiffSpan.TotalMilliseconds;

							
							/*
							
							TimeSpan timeDiffSpan2 = DateTime.Now - sharedData.prevChkTime;
							double timeDiff2 = timeDiffSpan2.TotalSeconds;
							TimeSpan timeDiffSpan3 = recieveTime - sharedData.prevServTime;
							double timeDiff3 = timeDiffSpan3.TotalSeconds;

							//만약 서버에서 보낸 이전 시간과 지금 시간이 1초 이상 차이날 경우
							//& 내 마지막 처리시간과도 0.5초 이상 차이날때
							if (timeDiff3 > 1.0f)
							{
								//해당 시간동안 처리한 (현재 시점 총패킷처리수 - 1초전 총처리패킷수 ) / 인원 를 표기한다.
								float allNum = (tmp.allPackNum - sharedData.allPacketNumPrev) / sharedData.allUserNum;
								sharedData.allPacketNumPrev = tmp.allPackNum;
								sharedData.prevChkTime = DateTime.Now;
								sharedData.prevServTime = recieveTime;

								//lock (Form1.Instance.msgList)
								{
									//Form1.Instance.msgList.Enqueue(string.Format("allpack:{0} ! \n", allNum));
								}
							}
							*/

							/*
							//500 번당 평균 계산
							if (msgChkNum >= 4999)
							{
								double average = 0;
								for (int i = 0; i < 5000; i++)
								{
									average += msgChkNums[i];
								}
								if (average > 0)
								{
									average = average / 5000;
								}

								//lock (Form1.Instance.msgList)
								{
									//Form1.Instance.msgList.Enqueue(string.Format("time:{0} ms \n", average));
								}

								msgChkNum = 0;
							}
							else
							{
								msgChkNums[msgChkNum] = timeDiff;
								msgChkNum += 1;
							}
							*/
							
						}

						break;
				}
			}
		}


		public void Disconnect()
		{
			throw new NotImplementedException();
		}

	

		public void OnRemoved()
		{
			Form1.remove_user(this);
			throw new NotImplementedException();
		}

		public void Send(Packet msg)
		{
			_token.Send(msg);
		}
	}
}
