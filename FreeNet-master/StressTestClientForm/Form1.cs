﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Threading;

using System.Net;
using System.Net.Sockets;

using Networking;

namespace StressTestClientForm
{
	public partial class Form1 : Form
	{

		//static ManualResetEvent manualEvent = new ManualResetEvent(false);
		//public string conIp = "13.209.41.158";
		public string conIp = "127.0.0.1";

		static List<IPeer> game_servers = new List<IPeer>();

		public static StressSharedData sharedData;
		public static NetworkService service = new NetworkService();
		long allPackNum = 0;
		static Form1 _instance = null;
		public Queue<string> msgList = new Queue<string>();
		//private static 인스턴스 객체
		public static Form1 Instance { get 
			{
				return _instance; 
			} }

		public Form1()
		{
			_instance = this;
			sharedData = new StressSharedData();
			InitializeComponent();
			// CNetworkService객체는 메시지의 비동기 송,수신 처리를 수행한다.
			// 메시지 송,수신은 서버, 클라이언트 모두 동일한 로직으로 처리될 수 있으므로
			// CNetworkService객체를 생성하여 Connector객체에 넘겨준다.
			var service = new NetworkService();
			Thread myThread = new Thread(ThrFunc);
			myThread.Start();

		}

		private void Form1_Load(object sender, EventArgs e)
		{
			//ThrFunc();

		}
		/// <summary>
		/// 접속 성공시 호출될 콜백 매소드.
		/// </summary>
		/// <param name="server_token"></param>
		static void on_connected_gameserver(UserToken token)
		{
			lock (game_servers)
			{
				IPeer server = new CRemoteServerPeer(token , sharedData);
				game_servers.Add(server);
				Console.WriteLine("Connected!");
			}
		}
	
		public void ThrFunc()
		{
		

			while (true)
			{
				lock (game_servers)
				{
					for (int i = 0; i < game_servers.Count(); i++)
					{
						lock (((CRemoteServerPeer)game_servers[i]).UserLock)
						{


							allPackNum += 1;
							Packet msg = Packet.Create((short)PROTOCOL.CHAT_MSG_REQ);

							//msg.Push("testtest");

							// /*
							Proto.cs_login tmp = new Proto.cs_login();
							tmp.UserPwd = "asdfasdf";

							tmp.allPackNum = allPackNum / 10;
							DateTime nowTime = DateTime.Now;
							//처리시간 계산
							Proto.customTime myTime = new Proto.customTime()
							{
								year = nowTime.Year,
								month = nowTime.Month,
								day = nowTime.Day,
								hour = nowTime.Hour,
								minute = nowTime.Minute,
								second = nowTime.Second,
								millisecond = nowTime.Millisecond,
							};
							tmp.timeTickC = myTime;

							// */

							msg.Push(tmp);

							game_servers[i].Send(msg);
						}
					}
				}
				Thread.Sleep(30);
			}
			
		}
		//폼 닫을때 실행
		private void Main_FormClosing(object sender, FormClosingEventArgs e)
		{
			lock (game_servers) 
			{
				for (int i = 0; i < game_servers.Count(); i++)
				{
					((CRemoteServerPeer)game_servers[i]).Disconnect();
				}
			}
		}


		private void textBox1_TextChanged(object sender, EventArgs e)
		{

		}
		static void OnConnectionSuccess(UserToken serverToken)
		{
			lock (game_servers)
			{
				IPeer serverPeer = new CRemoteServerPeer(serverToken, sharedData);
				game_servers.Add(serverPeer);

				Form1.Instance.Invoke(new Action(() => {
					Form1.Instance.listBox1.Items.Add(serverPeer);
				}));

			}
			
			
		}
		public static void remove_user(CRemoteServerPeer user)
		{
			lock (game_servers)
			{
				game_servers.Remove(user);
			}
		}
		static void OnConnectionFailed()
		{
			//manualEvent.Set();
		}

		private void button5_Click(object sender, EventArgs e)
		{
			lock (msgList)
			{
				int maxCount = 0;
				for (int i = 0; i < msgList.Count; i++)
				{
					maxCount += 1;
					textBox1.AppendText(msgList.Dequeue());
					if (maxCount > 100)
					{
						msgList.Clear();
						break;
					}
				}
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			var connector = new Connector(service);
			// 접속에 성공했을 때 호출되는 콜백
			connector.OnSuccess += OnConnectionSuccess;
			// 접속에 실패했을 때 호출되는 콜백
			connector.OnFailed += OnConnectionFailed;

			IPAddress ipAddress;
			if (!IPAddress.TryParse(conIp, out ipAddress))
			{
				ipAddress = Dns.GetHostAddresses(conIp)[0];
			}
			IPEndPoint endpoint = new IPEndPoint(ipAddress, 7979);
			connector.Connect(endpoint);


		}

		private void button4_Click(object sender, EventArgs e)
		{
			for (int i = 0; i < 40; i++)
			{
				var connector = new Connector(service);
				// 접속에 성공했을 때 호출되는 콜백
				connector.OnSuccess += OnConnectionSuccess;
				// 접속에 실패했을 때 호출되는 콜백
				connector.OnFailed += OnConnectionFailed;

				IPAddress ipAddress;
				if (!IPAddress.TryParse(conIp, out ipAddress))
				{
					ipAddress = Dns.GetHostAddresses(conIp)[0];
				}
				IPEndPoint endpoint = new IPEndPoint(ipAddress, 7979);
				connector.Connect(endpoint);
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{

		}

		public Form2 girdWindow;

		private void listView1_SelectedIndexChanged(object sender, EventArgs e)
		{
			//propertyGrid1.SelectedObject = (CRemoteServerPeer)game_servers[0];

			//girdWindow.propertyGrid1.SelectedObject = sender;
			//girdWindow.propertyGrid1.SelectedObject = (CRemoteServerPeer)game_servers[0];
			if (girdWindow == null)
			{
				girdWindow = new Form2();
			}

			if (listBox1.SelectedIndex >= 0 && game_servers.Count() > listBox1.SelectedIndex)
			{
				girdWindow.propertyGrid1.SelectedObject = (CRemoteServerPeer)game_servers[listBox1.SelectedIndex];
				//propertyGrid1.SelectedObject = (CRemoteServerPeer)game_servers[listBox1.SelectedIndex];
			}
			girdWindow.ShowDialog();

			girdWindow.propertyGrid1.Refresh();
		}
	}


}
