﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace StressTestClientForm
{
	public enum PROTOCOL : short
	{
		BEGIN = 0,

		CHAT_MSG_REQ = 1,
		CHAT_MSG_ACK = 2,

		END
	}

	public class Proto 
	{
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct cs_login
		{
			//[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 21)]
			//public string UserID;

			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 21)]
			public string UserPwd;

            public customTime timeTickS;
            public customTime timeTickC;

            public long allPackNum;

            //public int Channel;                    // 채널

			//public int Version;                    // 버전

			//[MarshalAs(UnmanagedType.ByValArray, SizeConst = 15)]
			//public char[] MacAddress;       // 0715
		}

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct cs_ping
        {
            public PACKET_HEADER ph;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct sc_ping // 12
        {
            public PACKET_HEADER ph;
            public long time;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct cs_room_ready
        {
            public PACKET_HEADER ph;
            public int UserIdx;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct cs_game_move
        {
            public PACKET_HEADER ph;
            public gamerstate state;

        }
        [System.Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct svector3
        {
            public Int16 x;
            public Int16 y;
            public Int16 z;
        }
        [System.Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct svector4
        {
            public Int16 x;
            public Int16 y;
            public Int16 z;
            public Int16 w;
        }

        [System.Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PACKET_HEADER
        {
            public UInt16 PacketLength;
            public UInt16 PacketId;
            public char Type; //압축여부 암호화여부 등 속성을 알아내는 값
        };
        [System.Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct conSuccess
        {
            public PACKET_HEADER hEADER;
            public int userIdx;
        }
        [System.Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct gamerstate
        {
            public int userIdx;
            public svector3 tankPos;
            public svector3 tankBodyQuaternion;
            public svector3 tankHeadQuaternion;
        }
        [System.Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct gamershotstart
        {
            public int userIdx; // 쏜놈 idx 4
            public UInt16 shotIdx; // 고유 idx 2
            public UInt16 shotSpeed; // 고유 idx 2
                                     //쏜 위치
            public svector3 shotPos;
            //쏜 각도
            public svector3 shotDegree;
            //쏜 시간
            public customTime timeTickC;
            public customTime timeTickS;
        }
        [System.Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct sc_game_move
        {
            public PACKET_HEADER ph;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public gamerstate[] state;
        }
        [System.Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct sc_game_init
        {
            public PACKET_HEADER ph;
            public int userIdx;

        }
        [System.Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct cs_game_shot
        {
            public PACKET_HEADER ph;
            public gamershotstart state;
        }

        [System.Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct sc_game_shot
        {
            public PACKET_HEADER ph;
            public gamershotstart state;
        }
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct customTime //28
        {
            public int year;
            public int month;
            public int day;
            public int hour;
            public int minute;
            public int second;
            public int millisecond;
        }

        [System.Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct cs_game_hit
        {
            public PACKET_HEADER ph;

            public UInt16 shotIdx; // 고유 idx 2
            public int shotUserIdx; // 쏜 사람
                                    //맞은 위치
            public svector3 shotPos;
            //물리력
            public UInt16 physicFos; // 고유 idx 2
                                     //맞은 시간
            public customTime timeTickC;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public int[] userIdx; // 맞은넘 idx
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public int[] damage; // 데미지
        }

        [System.Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct sc_game_hit
        {
            public PACKET_HEADER ph;
            public UInt16 shotIdx; // 고유 idx 2
            public int shotUserIdx; // 쏜 사람
                                    //맞은 위치
            public svector3 shotPos;
            //물리력
            public UInt16 physicFos; // 고유 idx 2
                                     //맞은 시간
            public customTime timeTickC;
            public customTime timeTickS;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public int[] userIdx; // 맞은넘 idx
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public int[] damage; // 데미지
        }

    }
}
