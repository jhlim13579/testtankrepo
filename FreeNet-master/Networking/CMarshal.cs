﻿using System;
using System.Collections.Generic;
using System.Text;



using System.Runtime.InteropServices;

namespace Networking
{
    public static class CMarshal
    {
        public static int GetSize(object _object)
        {
            return Marshal.SizeOf(_object);
        }

        public static int GetSize<T>()
        {
            return Marshal.SizeOf(typeof(T));
        }
        //패킷 구조체를 바이트로 변환
        public static byte[] Serialize(object _object)
        {
            int iSize = Marshal.SizeOf(_object);
            byte[] searializeBuffer = new byte[iSize];
            
            IntPtr ptr = Marshal.AllocHGlobal(iSize);
            Marshal.StructureToPtr(_object, ptr, false);
            Marshal.Copy(ptr, searializeBuffer, 0 , iSize);
            Marshal.FreeHGlobal(ptr);

            /*
            unsafe
            {
                fixed (byte* fixed_Buffer = searializeBuffer)
                    Marshal.StructureToPtr(_object, (IntPtr)fixed_Buffer, false);
            }
            */

            return searializeBuffer;


        }
        //바이트배열을 패킷 구조체로 변환
        public static T Deserialize<T>(byte[] _byte, int offset = 0)
        {
            int objectSize = GetSize<T>();

            if (objectSize > _byte.Length)
            {
                throw new Exception();
            }
            IntPtr buffer = Marshal.AllocHGlobal(objectSize);

            Marshal.Copy(_byte, offset, buffer, objectSize);
            object returnobj = Marshal.PtrToStructure<T>(buffer);
            Marshal.FreeHGlobal(buffer);
            //object returnobj = null;
            //Marshal.PtrToStructure(buffer, returnobj);
            return (T)returnobj;

        }


    }
}
