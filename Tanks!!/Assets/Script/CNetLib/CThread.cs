using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Runtime.InteropServices;
/// <summary>
/// 쓰레드 클래스 : 비동기 소켓 연결 , 처리 해주는 역할
/// </summary>
public class CThread
{
	public Socket m_Sock;



	private byte[] m_ReceiverBuff = new byte[40960];

	/// <summary>
	/// 서버에서 보내는 패킷을 체크하기 위한 쓰레드
	/// </summary>
	private Thread clientReceiveThread;
	private Thread clientSendThread;

	/// <summary>
	/// 동기화를 위한 락 오브젝트
	/// </summary>
	private object lockObj;
	public bool isNetStart = false;



	/// <summary>
	/// 인스턴스 : 서버에 연결 , 커넥션 시도 , 리시브 쓰레드 실행
	/// </summary>
	public CThread(Socket socket , Object _lockObj)
	{
		lockObj = _lockObj;
			try
			{
				clientReceiveThread = new Thread(new ThreadStart(ListenForData));
				clientSendThread = new Thread(new ThreadStart(SendData));
				clientReceiveThread.Start();
				clientSendThread.Start();

			
				m_Sock = socket;
				CNetPostBox.GetInstance.server_on = true;
				isNetStart = true;
			}
			catch (Exception e)
			{
				//Debug.Log(e);
			}
	

	}

	/// <summary>
	/// 락 오브젝트 지정 
	/// </summary>
	/// <param name="obj"></param>
	public void SetLock(object _obj)
	{
		lockObj = _obj;
	}

	/// <summary>
	/// 유니티 종료시 실행 쓰레드 꺼준다. 안끄면 계속 실행됨.
	/// </summary>
	public void Application_End()
	{
		/*
        if (CNetPostBox.GetInstance.server_on == true)
        {
            CPacket.cs_logout packet = new CPacket.cs_logout();
            packet.ph.m_Header_Size = Marshal.SizeOf(packet); // 89
            packet.ph.m_Header_Type = 1002;
           //
            byte[] mybytes = CMarshal.Serialize(packet);
            m_Sock.Send(mybytes, 0, mybytes.Length, SocketFlags.None);
        }
        */
		m_Sock.Disconnect(false);
		clientReceiveThread.Abort();
		clientSendThread.Abort();
		isNetStart = false;

	}


	/// <summary>
	/// clientReceiveThread에서 주기적으로 호출하는 메서드 : 데이터를 받았을시 큐에 넣어준다.
	/// </summary>
	private void ListenForData()
	{

		int m_Receive_Length = 0;

		//try
		{
			while (true)
			{
				m_Receive_Length = m_Sock.Receive(m_ReceiverBuff, 0, m_ReceiverBuff.Length, SocketFlags.None);

				if ((m_Receive_Length != 0))
				{
					var incommingData = new byte[m_Receive_Length];
					Array.Copy(m_ReceiverBuff, 0, incommingData, 0, m_Receive_Length);
					lock (lockObj)
					{
						CNetPostBox.GetInstance.PushData(incommingData);
					}
				}
				//Thread.Sleep(10);
			}

		}
		//catch (Exception e)
		{
		//	Console.WriteLine(e.ToString());
		}
	}

	private void SendData()
	{

		int m_Receive_Length = 0;

		try
		{
			while (true)
			{
				lock (lockObj)
				{
					while (CNetPostBox.GetInstance.sendQueue.Count > 0)
					{
						byte[] tmpbyte;
						lock (lockObj)
						{
							tmpbyte = CNetPostBox.GetInstance.GetSendData();
						}
						m_Sock.Send(tmpbyte, SocketFlags.None);
					}
				}

				Thread.Sleep(100);
			
			}

		}
		catch (Exception e)
		{
			Console.WriteLine(e.ToString());
		}
	}
}

