using System;
using System.Net;
using System.Text;
using System.Net.Sockets;
using System.Collections;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


using UnityEditor;

public class CNetManager : MonoBehaviour
{
    //락용 오브젝트
    public object lockObject = new object();
    //리시브 버퍼
    private byte[] receiverBuffsaved = new byte[40960];

    //해더 보다 오버되게 온 것들을 다시 리시브 버퍼에 0부터 옮겨닮기 위한 버퍼
    private byte[] header_Over_Save_Buffer = new byte[40960];

    public Socket sock;
    //public TcpClient tcpClient;


    public bool whileparse; // 파싱하는 동안 읽지못하게 막음
    public bool headerreset = false;
    public int myreceivelength; // 바이트 얼마만큼 받았는지
	private CThread mynet;
    public CLobbyManager cLobbyManager;
    public CGameManager cGameManager;
    public CPacket.playerinfo playerinfo;
	private CNetPostBox mypost;

    public GameObject testObj;

    private static CNetManager instance;

    public static CNetManager GetInstance
    {
        get
        {
            return instance;
        }
    }
    private void Awake()
	{
        instance = this;
    }

	public void Send(byte[] sendbytes)
    {
        sock.Send(sendbytes, 0, sendbytes.Length, SocketFlags.None);
    }

    // Start is called before the first frame update
    async void Start()
    {
        try
        {
            sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream , ProtocolType.Tcp);
            //tcpClient = new TcpClient();
            //tcpClient.Connect(IPAddress.Parse("127.0.0.1"), Convert.ToInt32("7001"));

            var ep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), Convert.ToInt32("7001"));


            bool connected = false;
            Task result = sock.ConnectAsync(ep);
            int index = Task.WaitAny(new[] { result }, 2000);
            connected = sock.Connected;
            if (!connected)
            {
                Debug.Log("에러!");
                sock.Close();
            }
			else
			{
                Debug.Log("연결!");
            }


            //mynet = new CThreadStream(tcpClient);
            mynet = new CThread(sock , lockObject);
            //mynet.SetLock(lockObject);
        
            
        }
        catch
        {
            Debug.Log("서버와의 연결이 되지 않습니다!");
        }
            //Invoke("End_Client", 1.0f);

        mypost = CNetPostBox.GetInstance;
        
        Send_Login();
        StartCoroutine(deque_recive());
    
       // Send_Get_SafeBox();

    }



    public void Send_Login()
    {
        Debug.Log("send login!");
        CPacket.cs_login packet = new CPacket.cs_login();
        int size = Marshal.SizeOf(typeof(CPacket.cs_login));
        packet.ph.PacketLength = (UInt16)size;
        packet.ph.PacketId = (UInt16)CPacket.PACKET_INFO.cs_login;
        CNetPostBox.GetInstance.PushSendData(CMarshal.Serialize(packet));
    }
    public void Send_RoomMake()
    {
        CPacket.cs_room_make packet = new CPacket.cs_room_make();
        int size = Marshal.SizeOf(typeof(CPacket.cs_room_make));
        packet.ph.PacketLength = (UInt16)size;
        packet.ph.PacketId = (UInt16)1102;
        packet.RoomTitle = "한 판 할 사람!";
        CNetPostBox.GetInstance.PushSendData(CMarshal.Serialize(packet));

    }
    public void onCompleteWriteToClientStream(IAsyncResult ar)
    {
      //  tcpClient.GetStream().EndWrite(ar);
    }
    private IEnumerator deque_recive()
    {
        while (true) // 큐가 1 보다 많을시
        {
            //if (whileparse == false)
            {
                lock (lockObject)
                {
                    if (CNetPostBox.GetInstance.messageQueue.Count > 0 && whileparse == false)
                    {
                        whileparse = true;
                        // 큐 데이터를 버퍼 끝지점(receiverBuffsaved + myreceivelength )에다 옮겨준다.
                        byte[] a = CNetPostBox.GetInstance.GetData();
                        Array.Copy(a, 0, receiverBuffsaved, myreceivelength, a.Length);
                        myreceivelength += a.Length;
                        Receives(); // 제일 선행된걸 불러줌
                    }
                }


            }
            yield return new WaitForSeconds(0.02f);
        }
    }
    byte[] m_bytes = new byte[40960];
    // byte[] header_temp_Buffer = new byte[8];

    //파싱이 수행되는 지점
    public void Receives()
    {
        //try
        {
            if (myreceivelength >= 5) // 5바이트 이상일시
            {
                Array.Clear(m_bytes, 0, 40960);
                CPacket.PACKET_HEADER _m_header = new CPacket.PACKET_HEADER();
                if (headerreset == false)
                {
                    headerreset = true;
                    _m_header.PacketLength = BitConverter.ToUInt16(receiverBuffsaved, 0);
                    _m_header.PacketId = BitConverter.ToUInt16(receiverBuffsaved, 2);
                }

                // 해더보다 작을 때 그냥 놔둠
                if (_m_header.PacketLength > myreceivelength)
                {
                    // Debug.Log("해더보다 작음 ");
                    whileparse = false;
                }
                // 해더보다 클때
                else if (_m_header.PacketLength < myreceivelength)
                {
                    Array.Copy(receiverBuffsaved, 0, m_bytes, 0, _m_header.PacketLength);
                     my_behavior(m_bytes);
                    // 해더보다 오버된 양만큼 header_Over_Save_Buffer에 저장후 다시 receiverBuffsaved에 옮겨준다.
                    Array.Copy(receiverBuffsaved, _m_header.PacketLength, header_Over_Save_Buffer, 0, myreceivelength - _m_header.PacketLength);
                    Array.Copy(header_Over_Save_Buffer, 0, receiverBuffsaved, 0, header_Over_Save_Buffer.Length);

                    myreceivelength -= (int)_m_header.PacketLength; // 해더만큼 뺀다
                    headerreset = false;
                    Receives();

                }
                // 해더길이와 동일
                else if (_m_header.PacketLength == myreceivelength)
                {
                    Array.Copy(receiverBuffsaved, 0, m_bytes, 0, _m_header.PacketLength);
                    //   Debug.Log("해더와 동일 :" + _m_header.m_Header_Size);
                    myreceivelength -= (int)_m_header.PacketLength;
                    headerreset = false;
                    my_behavior(m_bytes);
                    whileparse = false;
                }

            }
            // 5바이트 아래일시 그냥 놔둠
            else if (myreceivelength < 5)
            {
#if UNITY_EDITOR
                Debug.Log("5바이트 아래");
#endif
                whileparse = false;
            }

        }
        //  catch (Exception e)
        {
            //Debug.Log(e);
            //whileparse = false;
        }

    }

	private void Update()
	{
#if UNITY_EDITOR
        if (EditorApplication.isPlaying == false
            && mynet.isNetStart == true)
		{
          
            mynet.Application_End();

        }
#endif
    }

	void my_behavior(byte[] packet)
    {
        byte[] tempbuffer;
        // Array.Copy(packet, tempbuffer, 8);
        CPacket.PACKET_HEADER packetHeader = new CPacket.PACKET_HEADER();
        packetHeader.PacketLength = (UInt16)BitConverter.ToInt16(packet, 0);
        packetHeader.PacketId = (UInt16)BitConverter.ToInt16(packet, 2);
        Debug.Log("패킷수신! 사이즈:" + packetHeader.PacketLength + ", 타입:" + packetHeader.PacketId);


        if (packetHeader.PacketId == (UInt16)CPacket.PACKET_INFO.sc_login)
        {
            tempbuffer = new byte[Marshal.SizeOf(typeof(CPacket.sc_login))];
            Array.Copy(packet, tempbuffer, tempbuffer.Length);
            CPacket.sc_login sc_data = CMarshal.Deserialize<CPacket.sc_login>(tempbuffer, 0);
            playerinfo = sc_data.Info;
            if (sc_data.ErrorCode == 0)
            {
                Debug.Log("정상 로그인 프로세스 성공" + sc_data.Info.UserIdx);

            }
            else
            {

                Debug.Log("에러 메세지 :" + sc_data.ErrMsg);
            }
        }
        if (packetHeader.PacketId == (UInt16)CPacket.PACKET_INFO.sc_roomlist_info)
        {
            tempbuffer = new byte[Marshal.SizeOf(typeof(CPacket.sc_roomlist_info))];
            Array.Copy(packet, tempbuffer, tempbuffer.Length);
            CPacket.sc_roomlist_info sc_data = CMarshal.Deserialize<CPacket.sc_roomlist_info>(tempbuffer, 0);


			//컬 인포가 0이면 처음이고 초기화
			if (sc_data.CurInfo == 0)
			{
                cLobbyManager.ClearAllLobbyRoom();
			}

            for (int i = 0; i < 10; i++)
            {
                //if (sc_data.Info[i] != null)
                {
              
                    cLobbyManager.InstLobbyRoom(sc_data.Info[i]);

                /*
                    for (int i2 = 0; i2 < 10; i2++)
                    {
                        if (sc_data.Info[i].UserData[i2].RoomNo > 0)
                        {

                        }
                    }
                */

                }
            }
        



        }
        if (packetHeader.PacketId == (UInt16)CPacket.PACKET_INFO.sc_room_make)
        {
            tempbuffer = new byte[Marshal.SizeOf(typeof(CPacket.sc_room_make))];
            Array.Copy(packet, tempbuffer, tempbuffer.Length);
            CPacket.sc_room_make sc_data = CMarshal.Deserialize<CPacket.sc_room_make>(tempbuffer, 0);
            if (sc_data.ErrorCode == 0)
            {
                Debug.Log("정상 방 만들기 프로세스 성공" + sc_data.Info.RoomTitle);

            }
            else
            {

                Debug.Log("에러 메세지 :" + sc_data.ErrMsg);
            }
        }
        if (packetHeader.PacketId == (UInt16)CPacket.PACKET_INFO.sc_room_into)
        {
            Debug.Log("룸 인투 받음 ");
            tempbuffer = new byte[Marshal.SizeOf(typeof(CPacket.sc_room_into))];
            Array.Copy(packet, tempbuffer, tempbuffer.Length);
            CPacket.sc_room_into sc_data = CMarshal.Deserialize<CPacket.sc_room_into>(tempbuffer, 0);
            if (sc_data.ErrorCode == 0)
            {
                Debug.Log("정상 룸 진입 프로세스 성공" + sc_data.Info.RoomTitle);
                cLobbyManager.ClearAllRoomUsers();
                cLobbyManager.EnterRoomMain(sc_data);
            }
            else
            {

                Debug.Log("에러 메세지 :" + sc_data.ErrMsg);
            }
        }

        if (packetHeader.PacketId == (UInt16)CPacket.PACKET_INFO.sc_room_ready)
        {
            Debug.Log("룸 레디 받음 ");
            tempbuffer = new byte[Marshal.SizeOf(typeof(CPacket.sc_room_ready))];
            Array.Copy(packet, tempbuffer, tempbuffer.Length);
            CPacket.sc_room_ready sc_data = CMarshal.Deserialize<CPacket.sc_room_ready>(tempbuffer, 0);
            
            cLobbyManager.ReadyRecieve(sc_data);
          
        }

        if (packetHeader.PacketId == (UInt16)CPacket.PACKET_INFO.sc_room_start)
        {
            Debug.Log("룸 게임 스타트 받음 ");
            tempbuffer = new byte[Marshal.SizeOf(typeof(CPacket.sc_room_start))];
            Array.Copy(packet, tempbuffer, tempbuffer.Length);
            CPacket.sc_room_start sc_data = CMarshal.Deserialize<CPacket.sc_room_start>(tempbuffer, 0);

            cLobbyManager.RoomGameStart();

        }

        if (packetHeader.PacketId == (UInt16)CPacket.PACKET_INFO.sc_game_init)
        {
            Debug.Log("게임 init 받음 ");
            tempbuffer = new byte[Marshal.SizeOf(typeof(CPacket.sc_game_init))];
            Array.Copy(packet, tempbuffer, tempbuffer.Length);
            CPacket.sc_game_init sc_data = CMarshal.Deserialize<CPacket.sc_game_init>(tempbuffer, 0);

            cLobbyManager.GameInit(sc_data);
        }

        //나가기 성공
        if (packetHeader.PacketId == (UInt16)CPacket.PACKET_INFO.sc_room_out)
        {
            Debug.Log("룸 아웃 받음 ");
            tempbuffer = new byte[Marshal.SizeOf(typeof(CPacket.sc_room_out))];
            Array.Copy(packet, tempbuffer, tempbuffer.Length);
            CPacket.sc_room_out sc_data = CMarshal.Deserialize<CPacket.sc_room_out>(tempbuffer, 0);
            cLobbyManager.RoomOut();
        }

        //이동 관련 데이터 다 받음
        if (packetHeader.PacketId == (UInt16)CPacket.PACKET_INFO.sc_game_move)
        {
            tempbuffer = new byte[Marshal.SizeOf(typeof(CPacket.sc_game_move))];
            Array.Copy(packet, tempbuffer, tempbuffer.Length);
            CPacket.sc_game_move sc_data = CMarshal.Deserialize<CPacket.sc_game_move>(tempbuffer, 0);
            cGameManager.ReceiveAllMove(sc_data);
        }

        //히트 정보 수신
        if (packetHeader.PacketId == (UInt16)CPacket.PACKET_INFO.sc_game_hit)
        {
            tempbuffer = new byte[Marshal.SizeOf(typeof(CPacket.sc_game_hit))];
            Array.Copy(packet, tempbuffer, tempbuffer.Length);
            CPacket.sc_game_hit sc_data = CMarshal.Deserialize<CPacket.sc_game_hit>(tempbuffer, 0);
            cGameManager.ReceiveHitData(sc_data);
        }

        //나가기 성공
        if (packetHeader.PacketId == (UInt16)CPacket.PACKET_INFO.sc_room_out)
        {
            tempbuffer = new byte[Marshal.SizeOf(typeof(CPacket.sc_room_out))];
            Array.Copy(packet, tempbuffer, tempbuffer.Length);
            CPacket.sc_room_out sc_data = CMarshal.Deserialize<CPacket.sc_room_out>(tempbuffer, 0);
            cLobbyManager.OutRecieve(sc_data);
        }

        //나가기 성공
        /*
        if (packetHeader.PacketId == (UInt16)CPacket.PACKET_INFO.sc_game_solider)
        {
            tempbuffer = new byte[Marshal.SizeOf(typeof(CPacket.sc_game_soldier))];
            Array.Copy(packet, tempbuffer, tempbuffer.Length);
            CPacket.sc_game_soldier sc_data = CMarshal.Deserialize<CPacket.sc_game_soldier>(tempbuffer, 0);
            cGameManager.ReceiveAllMove(sc_data);
        }
        */
    }
}
