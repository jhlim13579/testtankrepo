using System.Collections.Generic;
using System.Collections.Concurrent;

/// <summary>
/// 싱글턴으로 만든 클래스를 하나두고 큐에 담아 리시브 데이터를 처리
/// </summary>
public class CNetPostBox
{
    //싱글턴 인스턴스
    private static CNetPostBox instance;

   // private readonly BlockingCollection<byte[]> ferQueue = new BlockingCollection<byte[]>(new ConcurrentQueue<byte[]>());


    /// <summary>
    /// 서버에서 온 리시브 데이터를 담는 큐
    /// </summary>
    public Queue<byte[]> messageQueue;
    public bool server_on = false;
    /// <summary>
    /// 현재 큐의 갯수
    /// </summary>
    public static int quenum;

    public Queue<byte[]> sendQueue;
    public static int quenum2;

    /// <summary>
    /// 싱글턴 인스턴스 반환
    /// </summary>
    public static CNetPostBox GetInstance
    {
        get
        {
            //인스턴스 없을시 생성 , 있을시 반환
            if (instance == null)
                instance = new CNetPostBox();

            return instance;
        }
    }

    /// <summary>
    /// 생성함수 : 큐를 초기화함.
    /// </summary>
    public CNetPostBox()
    {   
        messageQueue = new Queue<byte[]>();
        sendQueue = new Queue<byte[]>();
    }

    /// <summary>
    /// 리시브 큐를 반환한다.
    /// </summary>
    /// <returns></returns>
    public Queue<byte[]> rturn()
    {
        return messageQueue;
    }

    /// <summary>
    /// 리시브 큐를 반환한다.
    /// </summary>
    /// <returns></returns>
    public Queue<byte[]> sturn()
    {
        return sendQueue;
    }

    /// <summary>
    /// 큐에 데이터를 삽입한다.
    /// </summary>
    /// <param name="data"></param>
    public void PushData(byte[] data)
    {
        messageQueue.Enqueue(data);
    }

    public void PushSendData(byte[] data)
    {
        sendQueue.Enqueue(data);
    }

    /// <summary>
    /// 큐에있는 리시브 바이트 꺼내서 반환
    /// </summary>
    /// <returns></returns>
    public byte[] GetData()
    {
        byte[] tmpbytes;
        //데이타가 1개라도 있을 경우 꺼내서 반환
        if (messageQueue.Count > 0)
        {
            return messageQueue.Dequeue();
            /*
			if (messageQueue.TryDequeue(out tmpbytes))
			{
                return tmpbytes;
			}
			else
			{
                return default;
            }
            */
        }
        else
            return default;    //없으면 빈값을 반환
    }

    /// <summary>
    /// 큐에있는 리시브 바이트 꺼내서 반환
    /// </summary>
    /// <returns></returns>
    public byte[] GetSendData()
    {
        byte[] tmpbytes;
        //데이타가 1개라도 있을 경우 꺼내서 반환
        if (sendQueue.Count > 0)
        {
            return sendQueue.Dequeue();
            /*
            if (sendQueue.TryDequeue(out tmpbytes))
            {
                return tmpbytes;
            }
            else
            {
                return default;
            }
            */
        }
        else
            return default;    //없으면 빈값을 반환
    }
}

