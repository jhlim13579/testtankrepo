
using System;
using System.Runtime.InteropServices;

public static class CMarshal
{
  
    public static int GetSize(object _object)
    {
        return Marshal.SizeOf(_object);
    }

    public struct TypeSizeProxy<T>
    {
        public T PublicField;
    }
    public static int GetSize<T>()
    {
        try
        {
            return Marshal.SizeOf(typeof(T));
        }
        catch (ArgumentException)
        {
            return Marshal.SizeOf(new TypeSizeProxy<T>());
        }
    }

    /// <summary>
    /// 구조체-> 바이트 마샬링을 수행하는 스태틱 메서드 : 
    /// 구조체를 받아 바이트로 변환
    /// </summary>
    /// <param name="_object"></param>
    /// <returns></returns>
    public static byte[] Serialize(object _object)
    {
        byte[] serializeBuffer = new byte[Marshal.SizeOf(_object)];

        unsafe
        {
            fixed (byte * fixed_Buffer = serializeBuffer)
            {
                Marshal.StructureToPtr(_object, (IntPtr)fixed_Buffer, false);
            }
        }

        return serializeBuffer;
    }


    /// <summary>
    /// 마샬링을 수행하는 스태틱 메서드 :
    /// 구조체 타입을 받아 구조체 타입에 해당하는 연산수행
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="_byte"></param>
    /// <param name="_offset"></param>
    /// <returns></returns>
    /// 
    public static T Deserialize<T>(byte[] _byte , int _offset = 0)
    {
        int objectSize = GetSize<T>();


        //해당 구조체의 크기보다 높거나 낮은 바이트가 수신되었을시 마샬링 실패
        if (objectSize < _byte.Length)
        {
            Console.WriteLine("ms fail!");
            return default;
        }

        IntPtr buffer = Marshal.AllocHGlobal(objectSize);

        //앞에서 해당 크기만큼 복사
        Marshal.Copy(_byte, _offset, buffer, objectSize);

        object returnObject = Marshal.PtrToStructure<T>(buffer);

        Marshal.FreeHGlobal(buffer);

        return (T) returnObject;


    }

}
