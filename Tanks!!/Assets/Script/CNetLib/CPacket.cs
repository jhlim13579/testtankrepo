using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;




/// <summary>
/// 패킷 모음
/// </summary>
[StructLayout(LayoutKind.Sequential, Pack = 1)]

public class CPacket
{
    /// <summary>
    /// 패킷 번호
    /// 1000~ c->s
    /// 2000~ s->c
    /// 3000~ s->s 분산서버
    /// </summary>
    public enum PACKET_INFO
    {
        cs_login = 1100,
        cs_roomlist_info = 1101,
        cs_room_make = 1102,
        cs_room_into = 1103,
        cs_room_out = 1104,
        cs_room_ready = 1105,
        cs_room_start = 1106,

        cs_game_move = 1111,
        cs_game_hit = 1112,

        sc_login = 2100,
        sc_roomlist_info = 2101,
        sc_room_make = 2102,
        sc_room_into = 2103,
        sc_room_out = 2104,
        sc_room_ready = 2105,
        sc_room_start = 2106,

        sc_game_init = 2111,
        sc_game_move = 2112,
        sc_game_shot = 2113,
        sc_game_hit = 2114,
        sc_game_soldier = 2115,

    }


    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PACKET_HEADER
    {
        public UInt16 PacketLength;
        public UInt16 PacketId;
        public char Type; //압축여부 암호화여부 등 속성을 알아내는 값
    };

    [System.Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct playerinfo
    {
        public int RoomNo;
        public int RoomInIdx;
        public int UserIdx;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public byte[] UserID;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public byte[] Nick;

        public int Level;                      //4b  0: 정상 , etc:에러   81
        public int LvExp;  // 88
        public long Money;            // 머니			8    96
        public int SafeMoney;            // 머니		424

        public bool isReady;
        //탱크 데이터 
        public tankinfo tankInfo;


    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct tankinfo
    {
        public int bodyNum;
        public int mainGunNum;

    }


    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_login
    {
        public PACKET_HEADER ph;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 21)]
        public string UserID;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 21)]
        public string UserPwd;

        public int Channel;                    // 채널

        public int Version;                    // 버전

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 15)]
        public char[] MacAddress;       // 0715
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_logout
    {
        public PACKET_HEADER ph;

        public int ErrorCode;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_playerlist_info
    {
        public PACKET_HEADER ph;

        public int ErrorCode;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_roomlist_info
    {
        public PACKET_HEADER ph;
        public int ErrorCode;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_player_info
    {
        public PACKET_HEADER ph;
        public int UserIdx;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_user_info
    {
        public PACKET_HEADER ph;
        public int UserIdx;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_chat
    {
        public PACKET_HEADER ph;
        public int System;
        //한글 지원 캐릭터 사이즈x2
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
        public byte[] chat;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_room_make
    {
        public PACKET_HEADER ph;
        public int RoomNo;

        //한글 지원 캐릭터 사이즈x2
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 35)]
        public string RoomTitle;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string RoomPwd;
        public int Min_Money;
        public int Bet_Type;

    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_room_into
    {
        public PACKET_HEADER ph;
        public int RoomNo;


        public int Min_Money;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] RoomPwd;

    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_room_out
    {
        public PACKET_HEADER ph;
        public int RoomNo;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_room_info
    {
        public PACKET_HEADER ph;
        public int RoomNo;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_room_ready
    {
        public PACKET_HEADER ph;
        public int UserIdx;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_room_start
    {
        public PACKET_HEADER ph;
        public int UserIdx;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_game_init
    {
        public PACKET_HEADER ph;
        public int UserIdx;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_game_start
    {
        public PACKET_HEADER ph;
        public int UserIdx;
    }

    public struct SSQL_Q
    {
        public PACKET_HEADER header;
        public string query;
    }



    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_logout
    {
        public PACKET_HEADER header;
        public int ErrorCode;
        //한글 지원 캐릭터 사이즈x2
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 21)]
        public string ErrMsg;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_playerlist_info
    {
        public PACKET_HEADER header;
        public int MaxInfo;
        public int CurInfo;

        //수정필요
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public lobbyplayerinfo[] Info;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct lobbyplayerinfo
    {
        public int RoomNo;
        public int UserIdx;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public char[] UserID;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public byte[] Nick;
        public int Level;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_roomlist_info
    {
        public PACKET_HEADER header;
        public int MaxInfo;
        public int CurInfo;
        //10개씩 던지는걸로
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public roominfo[] Info;

    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct roominfo
    {
        public int RoomNo;
        public int ReadyTime;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] RoomTitle;

        //방장 넘버
        public int FirstRoomUser;

        //게임 시작 가능한 수 진영 수
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public int[] MinPlayUserNum;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public int[] NowPlayUserNum;

        public int NowUserNum;
        public int MaxUserNum;
        public bool isRoomPwd;   // true : 암호 있음, false : 없음.
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] RoomPwd;
        public bool Gaming;    // true : 게임중, false : 대기중

        //방에 있는 유저 목록 5:5로 10명까지
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        //public playerinfo[] UserData;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_login
    {
        public PACKET_HEADER ph;
        public int ErrorCode;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 41)]
        public string ErrMsg;
        public int MyChannel;
        public playerinfo Info;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_lobby_chat
    {
        public PACKET_HEADER ph;
        public int Color;
        //한글 지원 캐릭터 사이즈x2
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string Message;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_room_ready
    {
        public PACKET_HEADER ph;
        public int UserIdx;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] ErrMsg;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_room_update
    {
        public PACKET_HEADER ph;
        public int UserIdx;
        public int RoomNo;
        public roominfo Info;
        public int ErrorCode;
        //한글 지원 캐릭터 사이즈x2
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 21)]
        public string ErrMsg;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_room_make
    {
        public PACKET_HEADER ph;
        public int UserIdx;
        public int RoomNo;
        public roominfo Info;
        public int ErrorCode;
        //한글 지원 캐릭터 사이즈x2
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 21)]
        public string ErrMsg;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_room_into
    {
        public PACKET_HEADER ph;
        public int UserIdx;
        public int RoomNo;
        public roominfo Info;
        public int ErrorCode;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public playerinfo[] playerinfos;
        //한글 지원 캐릭터 사이즈x2
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] ErrMsg;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_room_move
    {
        public PACKET_HEADER ph;
        public int UserIdx;
        public int RoomNo;
        public roominfo Info;
        public int ErrorCode;
        //한글 지원 캐릭터 사이즈x2
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 21)]
        public string ErrMsg;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_room_leaving
    {
        public PACKET_HEADER ph;
        public int UserIdx;
        public bool Leving;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_room_out
    {
        public PACKET_HEADER ph;
        public int UserIdx;
        public int ErrorCode;
        //한글 지원 캐릭터 사이즈x2
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] ErrMsg;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_room_start
    {
        public PACKET_HEADER ph;
        public int waitTime;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public byte[] ErrMsg;
    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct svector3
    {
        public Int16 x;
        public Int16 y;
        public Int16 z;
    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct svector4
    {
        public Int16 x;
        public Int16 y;
        public Int16 z;
        public Int16 w;
    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct gamerstate
    {
        public int userIdx;
        public svector3 tankPos;
        public svector3 tankBodyQuaternion;
        public svector3 tankHeadQuaternion;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
        public gamershotstart[] shots;
    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct gamerstatus
    {
        public int userIdx;
        public UInt16 nowhp;
        public UInt16 maxhp;
        public UInt16 maxoil;
        public UInt16 nowoil;
    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct gamersoldier
    {
        public int userIdx; // 쏜놈 idx
        public int soldierIdx; // 고유 idx
        public int state; // 죽었는지 살았는지 상태로 판별
        public float hp;
        public float maxhp;
        public svector3 nowpos; //현재지점
        public svector3 nowdegree; //현재각도
    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct gamershotstart
    {
        public int userIdx; // 쏜놈 idx 4
        public UInt16 shotIdx; // 고유 idx 2

        public svector3 nowpos; //현재지점 6
                                //public svector3 startpos; //목표지점
                                //public svector3 destpos; //목표지점
                                //public svector3 shotdegree; //쏜각도
        public svector3 nowdegree; //현재각도 6

    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_game_soldier
    {
        public PACKET_HEADER ph;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public gamersoldier[] state;
    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_game_init
    {
        public PACKET_HEADER ph;
        public roominfo roomData;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public tankinfo[] tankinfos;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public playerinfo[] UserData;


    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_game_move
    {
        public PACKET_HEADER ph;
        public gamerstate state;

    }
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_game_move
    {
        public PACKET_HEADER ph;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public gamerstate[] state;


    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_game_shot
    {
        public PACKET_HEADER ph;
        public gamershotstart state;

    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct cs_game_hit
    {
        public PACKET_HEADER ph;
        public int useridx;
        public UInt16 shotidx;
        public svector3 hitPos;

    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_game_hit
    {
        public PACKET_HEADER ph;
        public int useridx;
        public UInt16 shotidx;
        public svector3 hitPos;


    }
    /*
    //Todo : 나중엔 탄환 수에 따라서 사이징되면서 바이트단위로 분해할수있도록
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct sc_game_shot
    {
        public PACKET_HEADER ph;
        //한놈당 20발씩 할당
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 200)]
        public gamershotstart[] shots;

    }
    */

}