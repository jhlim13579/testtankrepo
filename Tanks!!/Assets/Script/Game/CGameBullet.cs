using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.InteropServices;


[System.Serializable]
public class CGameBullet : MonoBehaviour
{
    public float speed = 1;
    public bool isMove;
    public bool isPlayer;

    public bool isHit;
    public CPacket.gamershotstart mGamershotstart;
    public GameObject mObjEffectHit;

    // Start is called before the first frame update
    void Start()
    {
        speed = 1f;
    }

    private void OnTriggerEnter(Collider other)
    {
       
        if (other != null)
        {
            if (other.tag == "Wall")
            {
                Debug.Log("hit wall!");
                isMove = false;
                isPlayer = false;
                CPacket.cs_game_hit packet = new CPacket.cs_game_hit();
                int size = Marshal.SizeOf(typeof(CPacket.cs_game_hit));
                packet.ph.PacketLength = (UInt16)size;
                packet.ph.PacketId = (UInt16)CPacket.PACKET_INFO.cs_game_hit;
                packet.useridx = mGamershotstart.userIdx;
                packet.shotidx = mGamershotstart.shotIdx;
                packet.hitPos = new CPacket.svector3
                {
                    x = Convert.ToInt16(Mathf.Clamp(gameObject.transform.position.x * 10f, -32760, 32760)),
                    y = Convert.ToInt16(Mathf.Clamp(gameObject.transform.position.y * 10f, -32760, 32760)),
                    z = Convert.ToInt16(Mathf.Clamp(gameObject.transform.position.z * 10f, -32760, 32760))
                };
                CNetPostBox.GetInstance.PushSendData(CMarshal.Serialize(packet));
               // CGameManager.Instance.cGameBullets.Remove( CGameManager.Instance.SearchBulletWithId(mGamershotstart.shotIdx) );
               // Destroy(this , 2);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (isMove == true && isPlayer == true)
        {
           gameObject.transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }

		if (isHit == true)
		{
        
            StartCoroutine(HitRoutine());

        }
    }


	public IEnumerator HitRoutine()
	{
        mObjEffectHit.SetActive(true);
        yield return new WaitForSeconds(2.1f);
        mObjEffectHit.SetActive(false);
        CGameManager.Instance.cGameBullets.Remove(CGameManager.Instance.SearchBulletWithId(mGamershotstart.shotIdx));
        Destroy(this.gameObject);


    }
}
