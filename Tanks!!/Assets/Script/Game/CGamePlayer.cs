using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.InteropServices;

public class CGamePlayer : MonoBehaviour
{

    public CPacket.gamerstate gamerstate;
    public GameObject mObjPlayer;
    public GameObject mObjPlayerBody;
    public GameObject mObjPlayerHead;
    public BoxCollider mBoxCollider;

  
    // Start is called before the first frame update
    void Start()
    {
     
    }

	private void OnTriggerEnter(Collider other)
	{
        CGameBullet bullet = other.GetComponent<CGameBullet>();

        if (bullet != null)
		{
            if (bullet.isHit == false && bullet.mGamershotstart.userIdx != gamerstate.userIdx)
			{
				Debug.Log("hit player!" + bullet.mGamershotstart.userIdx + "," + gamerstate.userIdx);

				bullet.isHit = true;

                CPacket.cs_game_hit packet = new CPacket.cs_game_hit();
                int size = Marshal.SizeOf(typeof(CPacket.cs_game_hit));
                packet.ph.PacketLength = (UInt16)size;
                packet.ph.PacketId = (UInt16)CPacket.PACKET_INFO.cs_game_hit;
                packet.useridx = bullet.mGamershotstart.userIdx;
                packet.shotidx = bullet.mGamershotstart.shotIdx;
                packet.hitPos = new CPacket.svector3 {
                    x = Convert.ToInt16(Mathf.Clamp(bullet.gameObject.transform.position.x * 10f, -32760, 32760)) ,
                    y = Convert.ToInt16(Mathf.Clamp(bullet.gameObject.transform.position.y * 10f, -32760, 32760)) ,
                    z = Convert.ToInt16(Mathf.Clamp(bullet.gameObject.transform.position.z * 10f, -32760, 32760)) 
                };


                CNetPostBox.GetInstance.PushSendData(CMarshal.Serialize(packet));

               // CGameManager.Instance.cGameBullets.Remove(CGameManager.Instance.SearchBulletWithId(bullet.mGamershotstart.shotIdx));
               // Destroy(bullet.gameObject , 2);
            }
		}
	}


	public void SetTank()
    {
        mObjPlayer = this.gameObject;
        mObjPlayerBody = this.gameObject.transform.GetChild(0).gameObject;
        mObjPlayerHead = this.gameObject.transform.GetChild(1).gameObject;
        mBoxCollider = mObjPlayerBody.GetComponent<BoxCollider>();
        mBoxCollider.isTrigger = true;
            
        

    }
 
}
