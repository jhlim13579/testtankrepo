﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoyStick1 : MonoBehaviour
{

    public GameObject mpStick = null;

    public bool mIsDrag = false;
    public bool mIsTurretJoystickDrag = false;

    public Vector3 mDir = Vector3.zero;

    private Vector3 mOriginPos = Vector3.zero;

    public GameObject mMoveCharacter = null;
    public Camera mPlayerCamera = null;
    public float mCameraRotationY = 0f;

    public float mFrontSpeed = 1.0f;

    public float mTurningAbility = 1.0f;

    public int mThisTouchCount = -1;

    public GameObject mCameraCenter = null;


    Touch mTouch;

    int mFingerID = 9999;


    // Start is called before the first frame update
    void Start()
    {
        if(mPlayerCamera != null)
        {
            mCameraCenter.transform.position = mMoveCharacter.transform.position;
            mPlayerCamera.transform.SetParent(mCameraCenter.transform);
        }

        mOriginPos = this.transform.position;
    }

    public void LeftBtnUp()
    {
        if(mMoveCharacter == null)
        {
            return;
        }
        mFingerID = 9999;
        mIsDrag = false;
    }
    public void LeftBtnDown()
    {
        if (mMoveCharacter == null)
        {
            return;
        }
#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            mFingerID = Input.touchCount - 1;
            Debug.Log("Left JoyStick mFingerID = " + mFingerID);
        }
#else

#endif
       
        mIsDrag = true;
    }

    public void RightBtnUp()
    {
        if (mMoveCharacter == null)
        {
            return;
        }
        Vector3 tVector = Vector3.zero;
#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
                tVector.x = mTouch.position.x;
                tVector.y = mTouch.position.y;
        }
#else
        tVector.x = Input.mousePosition.x;
        tVector.y = Input.mousePosition.y;
#endif
        Vector3 tTemp = (tVector - mOriginPos);
        mDir = tTemp.normalized;

        Vector3 tResult = Vector3.zero;
        if (tTemp.magnitude >= 50f)
        {
            Debug.Log("발사");
        }

        /*
        if (mOriginPos != mpStick.transform.position)
        {
            Debug.Log("발사");
        }
        */
        mFingerID = 9999;
        mIsTurretJoystickDrag = false;
    }
    public void RightBtnDown()
    {
        if (mMoveCharacter == null)
        {
            return;
        }
#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            mFingerID = Input.touchCount - 1;
            Debug.Log("Right JoyStick mFingerID = " + mFingerID);
        }
#else

#endif
        mIsTurretJoystickDrag = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (mMoveCharacter == null)
        {
            return;
        }

        /*
        if (true == Input.GetMouseButtonDown(0))
        {
            mIsDrag = true;
        }
        if (true == Input.GetMouseButtonUp(0))
        {
            mIsDrag = false;
        }
        */
        if (Input.touchCount >= mFingerID)
        {
            mTouch = Input.touches[mFingerID];
        }

        if (true == mIsDrag)
        {
            if (mCameraCenter != null)
            {
                mCameraCenter.transform.position = mMoveCharacter.transform.position;
            }

            Vector3 tVector = Vector3.zero;
#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
                tVector.x = mTouch.position.x;
                tVector.y = mTouch.position.y;
            }
#else
            tVector.x = Input.mousePosition.x; 
            tVector.y = Input.mousePosition.y;
#endif


            Vector3 tTemp = (tVector - mOriginPos);
            mDir = tTemp.normalized;

            Vector3 tResult = Vector3.zero;
            if (tTemp.magnitude >= 100f)
            {
                tResult = mOriginPos + mDir * 100f;
            }
            else
            {
                tResult = mOriginPos + mDir * tTemp.magnitude;
            }

            mpStick.transform.position = tResult;



            if (false == mDir.Equals(Vector3.zero))
            {
                mDir.z = mDir.y;
                mDir.y = 0;

                Quaternion v3Rotation = Quaternion.Euler(0f, mCameraRotationY, 0f);  // 회전각

                mDir = v3Rotation * mDir;


                mMoveCharacter.transform.rotation = Quaternion.RotateTowards(mMoveCharacter.transform.rotation, Quaternion.LookRotation(mDir, Vector3.up), mTurningAbility);


                mMoveCharacter.transform.Translate(mMoveCharacter.transform.forward * mFrontSpeed * Time.deltaTime, Space.World);

            }


        }
        else
        {
            mpStick.transform.position = Vector3.Lerp(mpStick.transform.position, this.transform.position, 20f * Time.deltaTime);

            mDir = Vector3.zero;
        }


        if (true == mIsTurretJoystickDrag)
        {
            Vector3 tVector = Vector3.zero;
#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
                tVector.x = mTouch.position.x;
                tVector.y = mTouch.position.y;
            }
#else
            tVector.x = Input.mousePosition.x;
            tVector.y = Input.mousePosition.y;
#endif

            Vector3 tTemp = (tVector - mOriginPos);
            mDir = tTemp.normalized;

            Vector3 tResult = Vector3.zero;
            if (tTemp.magnitude >= 100f)
            {
                tResult = mOriginPos + mDir * 100f;
            }
            else
            {
                tResult = mOriginPos + mDir * tTemp.magnitude;
            }

            mpStick.transform.position = tResult;



            if (false == mDir.Equals(Vector3.zero))
            {
                mDir.z = mDir.y;
                mDir.y = 0;

                Quaternion v3Rotation = Quaternion.Euler(0f, mCameraRotationY, 0f);  // 회전각

                mDir = v3Rotation * mDir;

                mMoveCharacter.transform.rotation = Quaternion.RotateTowards(mMoveCharacter.transform.rotation, Quaternion.LookRotation(mDir, Vector3.up), mTurningAbility);

            }
        }
        else
        {
            mpStick.transform.position = Vector3.Lerp(mpStick.transform.position, this.transform.position, 20f * Time.deltaTime);

            mDir = Vector3.zero;
        }



    }
}
