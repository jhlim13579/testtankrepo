using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using System.Text;
using System.Runtime.InteropServices;

public class CGameManager : MonoBehaviour
{
    private static CGameManager instance;
    public static CGameManager Instance
    {
        get
        {
            return instance;
        }
    }

  
    public CGamePlayer mObjTestBody;
    public CNetManager cNetManager;
    public GameObject mObjGameMain;
    public GameObject mObjGameUI;

    public CGamePlayer[] gamePlayers;
    public GameObject[] mObjGameUnitTank;
    public GameObject[] mObjGameUnitSolider;
    public CGamePlayer mCMyUnit;
    public CPacket.roominfo roomData;
    public CPacket.playerinfo[] playerinfos;
    public CPacket.gamerstate[] gamerstates;
    public CPacket.gamersoldier[] gamersoldiers;
    public Transform[] mPosInit;

    public JoyStick1[] mJoyStick;
    public GameObject mObjInstShot;
    public GameObject mObjInstShotParent;

    public List<CGameBullet> cGameBullets = new List<CGameBullet>();

    public bool isGameStart = false;
    public float testForce = 10;
    public int mNumShotActive;
	private void Awake()
	{
        instance = this;

    }
	// Start is called before the first frame update
	void Start()
    {
     
    }

    public CPacket.sc_game_move tmpMove;
    //솔져 전체 받음
    public void ReceiveAllSoldier(CPacket.sc_game_soldier msg) 
    {

        if (isGameStart == false)
        {
            Debug.Log("is game start  반환 " + isGameStart.ToString());
            return;
        }

        //솔져 하나씩 
        for (int i = 0; i < 10; i++)
        {
            if (msg.state[i].userIdx <= 0)
            {
                continue;
            }

            if (msg.state[i].userIdx == gamerstates[i].userIdx)
            {
                Debug.Log("gamer state update" + i);
                gamersoldiers[i] = msg.state[i];


                //나 자신이 아닌애들만 중계 받음
                if (msg.state[i].userIdx != cNetManager.playerinfo.UserIdx)
                {
                    mObjGameUnitSolider[i].transform.position =
                        new Vector3(msg.state[i].nowpos.x, msg.state[i].nowpos.y, msg.state[i].nowpos.z);

                    mObjGameUnitSolider[i].transform.eulerAngles =
                        new Vector3(msg.state[i].nowdegree.x, msg.state[i].nowdegree.y, msg.state[i].nowdegree.z);


                }
                else
                {

                  
                }
            }

        }




    }

    public void ReceiveHitData(CPacket.sc_game_hit msg)
    {
        Debug.Log("is game hit!" + msg.shotidx + "," + msg.useridx);
        if (isGameStart == false)
        {
            Debug.Log("is game hit  반환 " + isGameStart.ToString());
            return;
        }
        CGameBullet tmpbullet = new CGameBullet();
        if (msg.shotidx >= 0)
        {
            tmpbullet = SearchBulletWithId(msg.shotidx);
            //만약 해당 탄 아이디를가진 탄 못찾으면
            if (tmpbullet == null)
            {
                //생성해주고 업데이트
                CreateBulletOtherPlayer(new CPacket.gamershotstart { shotIdx = msg.shotidx, userIdx = msg.useridx, nowpos = msg.hitPos });
                UpdateBulletHitState(tmpbullet, msg);
            }
            //만약 해당 탄 아이디를가진 탄 찾으면 해당 위치로 업데이트
            else
            {
                UpdateBulletHitState(tmpbullet, msg);
            }
        }
    }

    public void ReceiveAllMove(CPacket.sc_game_move msg)
    {
        if (isGameStart == false)
        {
            Debug.Log("is game start  반환 " + isGameStart.ToString());
            return;
        }
        tmpMove = msg;
		
       // gamerstates = new CPacket.gamerstate[10];
        for (int i = 0; i < 10; i++)
        {
			if (msg.state[i].userIdx <= 0)
			{
                continue;
			}

			if (msg.state[i].userIdx == gamerstates[i].userIdx)
			{
                Debug.Log("gamer state update" + i);
                gamerstates[i] = msg.state[i];


                //나 자신이 아닌애들만 중계 받음
                if (msg.state[i].userIdx != cNetManager.playerinfo.UserIdx) 
                {
                    mObjGameUnitTank[i].transform.position =
                        new Vector3(msg.state[i].tankPos.x * 0.1f, msg.state[i].tankPos.y * 0.1f, msg.state[i].tankPos.z * 0.1f);

                    mObjGameUnitTank[i].transform.eulerAngles =
                        new Vector3(msg.state[i].tankBodyQuaternion.x * 0.1f, msg.state[i].tankBodyQuaternion.y * 0.1f, msg.state[i].tankBodyQuaternion.z * 0.1f);

                    gamePlayers[i].mObjPlayerHead.transform.eulerAngles =
                         new Vector3(msg.state[i].tankHeadQuaternion.x * 0.1f, msg.state[i].tankHeadQuaternion.y * 0.1f, msg.state[i].tankHeadQuaternion.z * 0.1f);

                    CGameBullet tmpbullet = new CGameBullet();
                    for (int i2 = 0; i2 < 5; i2++)
                    {
						if (msg.state[i].shots[i2].userIdx <= 0)
						{
                            Debug.Log("NO USER IDX!");
                            continue;
						}
                        if (msg.state[i].shots[i2].shotIdx > 0)
                        {
                            tmpbullet = SearchBulletWithId(msg.state[i].shots[i2].shotIdx);
                            //만약 해당 탄 아이디를가진 탄 못찾으면
                            if (tmpbullet == null)
                            {
                                Debug.Log( "not find bullet" + i2 + msg.state[i].shots[i2].shotIdx + "," + msg.state[i].shots[i2].userIdx);
                                CreateBulletOtherPlayer(msg.state[i].shots[i2]);
                            }
                            //만약 해당 탄 아이디를가진 탄 찾으면 해당 위치로 업데이트
                            else
                            {
                                Debug.Log("find bullet" + i2 + msg.state[i].shots[i2].shotIdx + "," + msg.state[i].shots[i2].nowpos.x + "," + msg.state[i].shots[i2].nowpos.y + "," + msg.state[i].shots[i2].nowpos.z);
                                UpdateBulletState(tmpbullet, msg.state[i].shots[i2]);
                            }
                        }
						else
						{
                            Debug.Log("NO SHOT IDX!");

                        }
                    }
                }
				else
				{

                    gamePlayers[i].gamerstate = msg.state[i];

                    mObjTestBody.gameObject.transform.position =
                        new Vector3(msg.state[i].tankPos.x * 0.1f, msg.state[i].tankPos.y * 0.1f, msg.state[i].tankPos.z * 0.1f);

                    mObjTestBody.transform.eulerAngles =
                        new Vector3(msg.state[i].tankBodyQuaternion.x * 0.1f, msg.state[i].tankBodyQuaternion.y * 0.1f, msg.state[i].tankBodyQuaternion.z * 0.1f);

                    mObjTestBody.mObjPlayerHead.transform.eulerAngles =
                         new Vector3(msg.state[i].tankHeadQuaternion.x * 0.1f, msg.state[i].tankHeadQuaternion.y * 0.1f, msg.state[i].tankHeadQuaternion.z * 0.1f);
                }
            }
        }
    
    }
    //idx 가 내것인지 체크하는 함수
    public bool UserOwnerChk(int idx) 
    {
        bool result = false;
		if (cNetManager.playerinfo.UserIdx == idx)
		{
            result = true;
		}
        return result;
    }

    public void UpdateBulletState(CGameBullet bullet , CPacket.gamershotstart msg)
    {
        bullet.mGamershotstart = msg;
        bullet.transform.position = new Vector3(msg.nowpos.x * 0.1f, msg.nowpos.y * 0.1f, msg.nowpos.z * 0.1f);
        bullet.transform.eulerAngles = new Vector3(msg.nowdegree.x * 0.1f, msg.nowdegree.y * 0.1f, msg.nowdegree.z * 0.1f);
    }
    public void UpdateBulletHitState(CGameBullet bullet, CPacket.sc_game_hit msg)
    {
		if (bullet == null)
		{
            return;
		}
		if (bullet.isHit == true)
		{
            return;
		}
        //만약 내것일시 내가 현재 쏜 수 내려주고
		if (UserOwnerChk(msg.useridx) && bullet.isHit == false)
		{
            Debug.Log("내가 쏜 탄!");
            if (mNumShotActive > 0)
            {
                mNumShotActive -= 1;
            }
        }
        bullet.isHit = true;
        bullet.transform.position = new Vector3(msg.hitPos.x * 0.1f, msg.hitPos.y * 0.1f, msg.hitPos.z * 0.1f);
    }
    public CGameBullet CreateBulletMyPlayer()
    {
        CGameBullet result = null;

        mNumShotActive += 1;
        GameObject tmpObj = Instantiate(mObjInstShot, mObjInstShotParent.transform);
        tmpObj.transform.position = mCMyUnit.mObjPlayerHead.transform.position;
        tmpObj.transform.eulerAngles = mCMyUnit.mObjPlayerHead.transform.eulerAngles;
        tmpObj.SetActive(true);
        tmpObj.GetComponent<CGameBullet>().isMove = true;
        tmpObj.GetComponent<CGameBullet>().isPlayer = true;
        tmpObj.GetComponent<CGameBullet>().mGamershotstart.shotIdx = (UInt16)(cNetManager.playerinfo.RoomInIdx * 5 + mNumShotActive + 1);
        tmpObj.GetComponent<CGameBullet>().mGamershotstart.userIdx = cNetManager.playerinfo.UserIdx;
        cGameBullets.Add(  tmpObj.GetComponent<CGameBullet>() );
        result = cGameBullets.Last();
        return result;
    }
    public CGameBullet CreateBulletOtherPlayer(CPacket.gamershotstart info)
    {
        CGameBullet result = null;

        GameObject tmpObj = Instantiate(mObjInstShot, mObjInstShotParent.transform);
        tmpObj.transform.position = new Vector3(-500, -500, -500);
        tmpObj.transform.eulerAngles = new Vector3(0, 0, 0);
        tmpObj.SetActive(true);
        tmpObj.GetComponent<CGameBullet>().isMove = true;
        cGameBullets.Add(tmpObj.GetComponent<CGameBullet>());
        cGameBullets.Last().mGamershotstart.shotIdx = (UInt16)info.shotIdx;
        cGameBullets.Last().mGamershotstart.userIdx = info.userIdx;
        result = cGameBullets.Last();
        return result;
    }
    public CGameBullet SearchBulletWithId(int shotIdx)
    {
        CGameBullet result = null;

        for (int i = 0; i < cGameBullets.Count(); i++)
        {
            if (cGameBullets[i].mGamershotstart.shotIdx == shotIdx)
            {
                result = cGameBullets[i];
                return result;
            }
        }
        return result;
    }
    public CGamePlayer SearchPlayerWithId(int useridx) 
    {
        CGamePlayer result = null;

		for (int i = 0; i < gamePlayers.Count(); i++)
		{
			if (gamePlayers[i].gamerstate.userIdx == useridx)
			{
                return result;
			}
		}
        return result;
    }

	public IEnumerator MoveRoutine()
	{
		while (true)
		{
            CPacket.cs_game_move packet = new CPacket.cs_game_move();
            int size = Marshal.SizeOf(typeof(CPacket.cs_game_move));
            packet.ph.PacketLength = (UInt16)size;
            packet.ph.PacketId = (UInt16)CPacket.PACKET_INFO.cs_game_move;
            packet.state = new CPacket.gamerstate();
            packet.state.userIdx = cNetManager.playerinfo.UserIdx;

            packet.state.tankPos.x = (Int16)Mathf.Clamp(mCMyUnit.mObjPlayer.transform.position.x * 10f , -32760 , 32760);
            packet.state.tankPos.y = (Int16)Mathf.Clamp(mCMyUnit.mObjPlayer.transform.position.y * 10f, -32760, 32760);
            packet.state.tankPos.z = (Int16)Mathf.Clamp(mCMyUnit.mObjPlayer.transform.position.z * 10f, -32760, 32760);

            packet.state.tankBodyQuaternion.x = (Int16)Mathf.Clamp(mCMyUnit.transform.eulerAngles.x* 10f, -32760 , 32760);
            packet.state.tankBodyQuaternion.y = (Int16)Mathf.Clamp(mCMyUnit.transform.eulerAngles.y* 10f, -32760 , 32760);
            packet.state.tankBodyQuaternion.z = (Int16)Mathf.Clamp(mCMyUnit.transform.eulerAngles.z * 10f, -32760, 32760);
            packet.state.tankHeadQuaternion.x = (Int16)Mathf.Clamp(mCMyUnit.mObjPlayerHead.transform.eulerAngles.x* 10f , -32760 , 32760);
            packet.state.tankHeadQuaternion.y = (Int16)Mathf.Clamp(mCMyUnit.mObjPlayerHead.transform.eulerAngles.y* 10f, -32760 , 32760);
            packet.state.tankHeadQuaternion.z = (Int16)Mathf.Clamp(mCMyUnit.mObjPlayerHead.transform.eulerAngles.z * 10f, -32760, 32760);

            packet.state.shots = new CPacket.gamershotstart[5];
			for (int i = 0; i < cGameBullets.Count(); i++)
			{
				if (cGameBullets[i] == null)
				{
                    continue;
				}
				if (cGameBullets[i].isHit)
				{
                    continue;
				}
                if (cGameBullets[i].isMove == true && cGameBullets[i].isPlayer == true)
                {
                    packet.state.shots[i].nowpos = new CPacket.svector3
                    {
                        x = (Int16)Mathf.Clamp(cGameBullets[i].transform.position.x * 10f, -32760, 32760),
                        y = (Int16)Mathf.Clamp(cGameBullets[i].transform.position.y * 10f, -32760, 32760),
                        z = (Int16)Mathf.Clamp(cGameBullets[i].transform.position.z * 10f, -32760, 32760)
                    };
                    packet.state.shots[i].nowdegree = new CPacket.svector3
                    {
                        x = (Int16)Mathf.Clamp(cGameBullets[i].transform.eulerAngles.x * 10f, -32760, 32760),
                        y = (Int16)Mathf.Clamp(cGameBullets[i].transform.eulerAngles.y * 10f, -32760, 32760),
                        z = (Int16)Mathf.Clamp(cGameBullets[i].transform.eulerAngles.z * 10f, -32760, 32760)
                    };
                    packet.state.shots[i].shotIdx = cGameBullets[i].mGamershotstart.shotIdx;
                    packet.state.shots[i].userIdx = cNetManager.playerinfo.UserIdx;
                    //cGameBullets[i].mGamershotstart.shotIdx = packet.state.shots[i].shotIdx;
                    //cGameBullets[i].mGamershotstart.userIdx = packet.state.shots[i].userIdx;
                }

            }

            //cNetManager.Send(CMarshal.Serialize(packet));
            CNetPostBox.GetInstance.PushSendData(CMarshal.Serialize(packet));
            yield return new WaitForSeconds(0.1f);
        }

	}

    // Update is called once per frame
    void Update()
    {
        //이동 패킷 테스트
		if (isGameStart == true)
		{
          
        }
    }
    //쏘기 기능 실행했을시
    public void GameShot()
    {
        //5개 까지 일단
		if (mNumShotActive >= 5)
		{
            return;
		}
        
        CreateBulletMyPlayer();
        //  Vector3 force = mCMyUnit.mObjPlayerHead.transform.eulerAngles * mCMyUnit.gamerstate.shotSpeed;
        //Vector3 force = new Vector3( mCMyUnit.mObjPlayerHead.transform.eulerAngles.x , 1 , mCMyUnit.mObjPlayerHead.transform.eulerAngles.z);
        //tmpObj.GetComponent<Rigidbody>().AddForce(force * testForce);

    }
    public void GameShotRecieve()
    {



    }


    public void OutRecieve(CPacket.sc_room_out msg)
    {
		if (isGameStart == true)
		{
            for (int i = 0; i < gamePlayers.Count(); i++)
            {
                if (gamePlayers[i].gamerstate.userIdx == msg.UserIdx)
                {
                    gamePlayers[i].mObjPlayer.SetActive(false);
                }
            }
        }
		else
		{
            return;
		}


    }
    public void GameInit(CPacket.sc_game_init msg)
    {
        mObjGameMain.SetActive(true);
        mObjTestBody.SetTank();
        //playerinfos = new CPacket.playerinfo[10];
        //gamerstates = new CPacket.gamerstate[10];

        //조이스틱 포함된 ui 켜줌
        mObjGameUI.SetActive(true);
        roomData = msg.roomData;
        Debug.Log(msg.roomData.NowUserNum);
        //Todo : 플레이의 수에 따라서 생성해주고 배치
        for (int i = 0; i < msg.roomData.NowUserNum; i++)
		{
            mObjGameUnitTank[i].SetActive(true);
            mObjGameUnitTank[i].transform.position
                = mPosInit[i].position;
            mObjGameUnitTank[i].GetComponent<CGamePlayer>().SetTank();
            CGamePlayer cGamePlayer = mObjGameUnitTank[i].GetComponent<CGamePlayer>();
            cGamePlayer.gamerstate.userIdx = msg.UserData[i].UserIdx;
            gamePlayers[i] = cGamePlayer;
            playerinfos[i] = msg.UserData[i];
            gamerstates[i].userIdx = msg.UserData[i].UserIdx;
        
            Debug.Log("이닛 루프 " + i + " user idx :" + msg.UserData[i].UserIdx);
            //내 조작 유닛 설정
			if (cNetManager.playerinfo.UserIdx == msg.UserData[i].UserIdx)
			{
                Debug.Log("내 유닛 찾음");
             
                mCMyUnit = cGamePlayer;
                cNetManager.playerinfo.RoomInIdx = msg.UserData[i].RoomInIdx;
                cNetManager.playerinfo.RoomNo = msg.UserData[i].RoomNo;

                mJoyStick[0].mMoveCharacter = cGamePlayer.gameObject;
                mJoyStick[1].mMoveCharacter = cGamePlayer.mObjPlayerHead;

            }

         
        }


        isGameStart = true;
        StartCoroutine(MoveRoutine());

    }
}
