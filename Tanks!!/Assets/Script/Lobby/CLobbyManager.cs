using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using System.Text;
using System.Runtime.InteropServices;
public class CLobbyManager : MonoBehaviour
{
    
    private static CLobbyManager instance;
    public static CLobbyManager Instance
    {
        get
        {
            //인스턴스 없을시 생성 , 있을시 반환
            if (instance == null)
                instance = new CLobbyManager();

            return instance;
        }
    }
    
    public CGameManager cGameManager;
    //로비 부분
    public GameObject mObjLobbyCanvas;
    public int mNumMaxRoom = 256;
    public List<CLobbyRoomUnit> cLobbyRoomUnits = new List<CLobbyRoomUnit>();
    public GameObject mObjInstLobbyRoomMain;
    public RectTransform mParentLoobyRoomMain;
    public GameObject mObjPointLobbyRoom;
    public CPacket.roominfo roominfo;
    public CNetManager cNetManager;



    //룸 부분
    public GameObject mObjRoomCanvas;
    public List<CRoomUnit> cRoomUnits = new List<CRoomUnit>();
    public GameObject mObjInstRoomUserMain;
    public RectTransform mParentRoomUserMain;
    public Text mTexRoomTitle;
    public GameObject mObjPointRoomUser;
    public GameObject mObjBtnGameStart;

    public Text mTexChatBox;

    public Color readyOkColor;
    public Color readyNoColor;
    


    public void InstLobbyRoom(CPacket.roominfo info)
    {
        mObjPointLobbyRoom = null;
        
        for (int i = 0; i < cLobbyRoomUnits.Count; i++)
		{
            if (i == info.RoomNo && info.MaxUserNum != 0)
			{
                cLobbyRoomUnits[i].roominfo = info;
                cLobbyRoomUnits[i].mTexTitle.text = Encoding.Unicode.GetString( info.RoomTitle );
                cLobbyRoomUnits[i].mTexUserNum.text = info.NowUserNum.ToString() + " / " + info.MaxUserNum.ToString();
                mObjPointLobbyRoom = cLobbyRoomUnits[i].gameObject;
              
                break;
            }
		}
        if (mObjPointLobbyRoom != null)
        {
            mObjPointLobbyRoom?.SetActive(true);
        }

    }

    public void InstRoomUser(CPacket.playerinfo info)
    {
        mObjInstRoomUserMain = null;
        Debug.Log("룸 유저 생성");
        for (int i = 0; i < cRoomUnits.Count; i++)
        {
            if (cRoomUnits[i].isIn == false)
            {
            Debug.Log("룸 유저 생성" + i);
                cRoomUnits[i].isIn = true;
                cRoomUnits[i].gameObject.SetActive(true);
                cRoomUnits[i].playerinfo = info;
                cRoomUnits[i].mTexName.text = Encoding.Unicode.GetString(info.UserID);
                mObjInstRoomUserMain = cLobbyRoomUnits[i].gameObject;
                break;
            }
        }
        if (mObjInstRoomUserMain != null)
        {
            mObjInstRoomUserMain?.SetActive(true);
        }
    }
    public void UpdateRoomUser(CPacket.playerinfo info , CRoomUnit roomUnit)
    {
        Debug.Log("룸 유저 업뎃");
        roomUnit.gameObject.SetActive(true);
        roomUnit.playerinfo = info;
    }

    public void ClearAllLobbyRoom()
    {
        for (int i = 0; i < cLobbyRoomUnits.Count; i++)
        {
            cLobbyRoomUnits[i].gameObject.SetActive(false);
            
        }
       

    }

	public void ClearAllRoomUsers() 
        {
        for (int i = 0; i < cRoomUnits.Count; i++)
        {
            cRoomUnits[i].gameObject.SetActive(false);
            cRoomUnits[i].isIn = false;
        }


    }

    // Start is called before the first frame update
    void Awake()
    {
		for (int i = 0; i < mNumMaxRoom; i++)
		{
            GameObject tmpObj = Instantiate(mObjInstLobbyRoomMain, mParentLoobyRoomMain);
            cLobbyRoomUnits.Add(tmpObj.GetComponent<CLobbyRoomUnit>());
            cLobbyRoomUnits.Last().cLobbyManager = this;
            tmpObj.SetActive(false);
        }
    }

    public CPacket.playerinfo[] tmpRoomUserInfos;
    //로비에서 룸으로 진입
    //로비 화면에서 룸 화면으로 넘어감
    public void EnterRoomMain(CPacket.sc_room_into msg)
    {
        mObjLobbyCanvas.SetActive(false);
        mObjRoomCanvas.SetActive(true);
        tmpRoomUserInfos = msg.playerinfos;

        //룸데이터를 세팅
        mTexRoomTitle.text = Encoding.Unicode.GetString(msg.Info.RoomTitle);

        Debug.Log("룸 엔터 받음!");

		//유저들 데이터 세팅
        for (int i = 0; i < 10; i++)
		{
            //유저 데이터가 존재한다면
			if (msg.playerinfos[i].UserIdx > 0)
			{
                Debug.Log("유저 idx 존재!");

                int tmpResult = -1;
                for (int i2 = 0; i2 < cRoomUnits.Count; i2++) 
                {
                    if (cRoomUnits[i2].isIn == true && cRoomUnits[i2].playerinfo.UserIdx == msg.playerinfos[i].UserIdx)
                    {
                        tmpResult = i2;
                    }
                }
				if (tmpResult == -1)
				{
                    InstRoomUser(msg.playerinfos[i]);
				}
				else
				{
                    UpdateRoomUser(msg.playerinfos[i], cRoomUnits[tmpResult]);
                }
            }
			else
			{
                Debug.Log("유저 idx 0!");
            }
		}


    }



    //해당 룸에서 나가기 요청했을시
    public void OutRoomMain()
    {
        CPacket.cs_room_out packet = new CPacket.cs_room_out();
        int size = Marshal.SizeOf(typeof(CPacket.cs_room_out));
        packet.ph.PacketLength = (UInt16)size;
        packet.ph.PacketId = (UInt16)CPacket.PACKET_INFO.cs_room_out;
        packet.RoomNo = roominfo.RoomNo;

        
        
        CNetPostBox.GetInstance.PushSendData(CMarshal.Serialize(packet));
    }
    public void OutRecieve(CPacket.sc_room_out msg)
    {
        //해당 유저 찾음
        for (int i = 0; i < cRoomUnits.Count(); i++)
        {
            if (cRoomUnits[i].playerinfo.UserIdx == msg.UserIdx)
            {

                cRoomUnits[i].isIn = false;
                cRoomUnits[i].gameObject.SetActive(false);
                break;
            }
        }


    }

    public void RoomOut()
    {
        mObjLobbyCanvas.SetActive(true);
        mObjRoomCanvas.SetActive(false);
        ClearAllRoomUsers();
        ClearAllLobbyRoom();
    }
    //해당 룸에서 레디 요청시
    public void ReadyClick()
    {
        CPacket.cs_room_ready packet = new CPacket.cs_room_ready();
        int size = Marshal.SizeOf(typeof(CPacket.cs_room_ready));
        packet.ph.PacketLength = (UInt16)size;
        packet.ph.PacketId = (UInt16)CPacket.PACKET_INFO.cs_room_ready;

        packet.UserIdx = cNetManager.playerinfo.UserIdx;

        CNetPostBox.GetInstance.PushSendData(CMarshal.Serialize(packet));
    }   
    
    //해당 유저 레디 받음
    public void ReadyRecieve(CPacket.sc_room_ready msg)
    {
        //해당 유저 찾음
		for (int i = 0; i < cRoomUnits.Count(); i++)
		{
			if (cRoomUnits[i].playerinfo.UserIdx == msg.UserIdx)
			{
                //색 바꿔준다.
                cRoomUnits[i].SetReady(readyOkColor);
                break;
			}
		}


    }

    //방 게임 시작 요청 3초뒤에 시작된다는 통보
    public void RoomGameStart() 
    {
        StartCoroutine(GameStartTexRoutine());
    }


	public IEnumerator GameStartTexRoutine()
	{
		for (int i = 3; i > 0; i--)
		{
            mTexChatBox.text += "\n" + "게임 시작" +  i + "초 전";
		    yield return new WaitForSecondsRealtime(0.99f);
        }

	}

    //해당 룸에서 게임 스타트 요청
    public void GameStartClick()
    {
        CPacket.cs_room_start packet = new CPacket.cs_room_start();
        int size = Marshal.SizeOf(typeof(CPacket.cs_room_start));
        packet.ph.PacketLength = (UInt16)size;
        packet.ph.PacketId = (UInt16)CPacket.PACKET_INFO.cs_room_start;

        packet.UserIdx = cNetManager.playerinfo.UserIdx;

        //cNetManager.Send(CMarshal.Serialize(packet));
        CNetPostBox.GetInstance.PushSendData(CMarshal.Serialize(packet));
    }
    //게임 시작 통지
    public void GameInit(CPacket.sc_game_init msg)
    {
        mObjLobbyCanvas.SetActive(false);
        mObjRoomCanvas.SetActive(false);
        ClearAllRoomUsers();
        ClearAllLobbyRoom();
        cGameManager.GameInit(msg);

    }
}
