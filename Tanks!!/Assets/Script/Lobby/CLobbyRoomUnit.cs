using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using System;

public class CLobbyRoomUnit : MonoBehaviour
{
    public CPacket.roominfo roominfo;
    public GameObject mObjMain;
    public Text mTexTitle;
    public Text mTexUserNum;
    public CLobbyManager cLobbyManager;

    public void SetTex()
    {
        //mTexTitle.text = roominfo.RoomTitle;
        //mTexUserNum.text = roominfo.NowUserNum.ToString() +"/" + roominfo.MaxUserNum.ToString();

    }


    public void ClickEnter()
    {
        Send_RoomEnter();
    }
    public void Send_RoomEnter()
    {
        CPacket.cs_room_into packet = new CPacket.cs_room_into();
        int size = Marshal.SizeOf(typeof(CPacket.cs_room_into));
        packet.ph.PacketLength = (UInt16)size;
        packet.ph.PacketId = (UInt16)CPacket.PACKET_INFO.cs_room_into;
        packet.RoomNo = roominfo.RoomNo;
        packet.RoomPwd = roominfo.RoomPwd;
        Debug.Log("send room into roomno:" + packet.RoomNo);
        CNetPostBox.GetInstance.PushSendData(CMarshal.Serialize(packet));
    }

    public void Clear()
    {
        mObjMain.SetActive(false);
        mTexTitle.text = "";
        mTexUserNum.text = "";
    }

    
}
